package myplayground.example.learningq.repository

import android.content.Context
import android.util.Log
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import myplayground.example.learningq.model.Customer
import myplayground.example.learningq.model.DeliveryOrder
import myplayground.example.learningq.model.Token
import myplayground.example.learningq.model.User
import myplayground.example.learningq.network.ApiService
import myplayground.example.learningq.network.request.LoginRequest
import myplayground.example.learningq.network.request.UpdateDeliveryLocationRequest
import myplayground.example.learningq.network.response.OSRMResponse
import myplayground.example.learningq.repository.paging.DeliveryOrderPagingSource

class FakeRepository(
    context: Context,
) : Repository {

    override suspend fun userLogin(request: UserLoginInput, apiService: ApiService): Token? {
        val response = apiService.login(
            LoginRequest(
                username = request.username,
                password = request.password,
            )
        )

        return Token(
            auth_token = response.data.accessToken,
            role = "",
        )
    }


    override suspend fun userMe(apiService: ApiService): User? {
        return apiService.userMe().data.user
    }

    override suspend fun fetchDeliveryOrderPaging(
        apiService: ApiService,
        status: String?,
        startDate: String?,
        endDate: String?,
    ): Flow<PagingData<DeliveryOrder>> {
        return Pager(
            config = PagingConfig(
                pageSize = 10,
                initialLoadSize = 10,
                prefetchDistance = 2,
            ),
            pagingSourceFactory = {
                DeliveryOrderPagingSource(
                    apiService,
                    status,
                    startDate,
                    endDate
                )
            }).flow

    }

    override suspend fun deliveryOrderUpdateDelivering(
        id: String,
        apiService: ApiService
    ): DeliveryOrder {
        return apiService.deliveryOrderUpdateDelivering(id)
            .let { DeliveryOrder.fromResponse(it.data.delivery_order!!) }
    }

    override suspend fun deliveryOrderCancel(
        id: String,
        apiService: ApiService,
    ): DeliveryOrder {
        return apiService.deliveryOrderCancel(id)
            .let { DeliveryOrder.fromResponse(it.data.delivery_order!!) }
    }

    override suspend fun deliveryOrderUpdateCompleted(
        id: String,
        apiService: ApiService
    ): DeliveryOrder {
        return apiService.deliveryOrderUpdateCompleted(id)
            .let { DeliveryOrder.fromResponse(it.data.delivery_order!!) }
    }

    override suspend fun deliveryOrderUpdatePosition(
        latitude: Double,
        longitude: Double,
        bearing: Double,
        apiService: ApiService
    ) {
        apiService.updateDeliveryLocation(
            UpdateDeliveryLocationRequest(
                latitude,
                longitude,
                bearing,
            )
        )
    }

    override suspend fun getDeliveryOrderById(
        deliveryOrderId: String,
        apiService: ApiService
    ): DeliveryOrder {
        return apiService.getDeliveryOrderById(deliveryOrderId).let {
            val data = it.data.delivery_order!!

            DeliveryOrder(
                id = data.id,
                supplier_id = data.supplier_id,
                user_id = data.user_id,
                invoice_number = data.invoice_number,
                date = data.date,
                status = data.status,
                total_price = data.total_price,
                created_at = data.created_at,
                updated_at = data.updated_at,
                customer = Customer.fromResponse(data.customer),
                items = data.items,
                images = data.images,
            )
        }
    }

    override suspend fun getActiveDeliveringDeliveryOrder(apiService: ApiService): DeliveryOrder? {
        return apiService.getActiveDeliveryOrder().let {
            if (it.data?.delivery_order == null) return null

            val data = it.data.delivery_order

            DeliveryOrder(
                id = data.id,
                supplier_id = data.supplier_id,
                user_id = data.user_id,
                invoice_number = data.invoice_number,
                date = data.date,
                status = data.status,
                total_price = data.total_price,
                created_at = data.created_at,
                updated_at = data.updated_at,
                customer = Customer.fromResponse(data.customer),
                items = data.items,
                images = data.images,
            )
        }
    }

    override suspend fun osrm(
        startLongitude: Double,
        destinationLongitude: Double,
        startLatitude: Double,
        destinationLatitude: Double,
        apiService: ApiService
    ): OSRMResponse {
        return apiService.osrm(
            startLongitude,
            destinationLongitude,
            startLatitude,
            destinationLatitude,
        )
    }

    companion object {
        @Volatile
        private var instance: FakeRepository? = null


        fun getInstance(
            context: Context,
        ): FakeRepository = instance ?: synchronized(this) {
            FakeRepository(
                context,
            ).apply {
                instance = this
            }
        }
    }
}
