package myplayground.example.learningq.repository.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import myplayground.example.learningq.model.Customer
import myplayground.example.learningq.model.DeliveryOrder
import myplayground.example.learningq.network.ApiService
import myplayground.example.learningq.network.request.FetchDeliveryOrderRequest
import retrofit2.HttpException
import java.io.IOException

class DeliveryOrderPagingSource(
    private val apiService: ApiService,
    val status: String? = null,
    val startDate: String? = null,
    val endDate: String? = null,
) : PagingSource<Int, DeliveryOrder>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, DeliveryOrder> {
        val currentPage = params.key ?: 1

        return try {
            val response = apiService.fetchDeliveryOrder(
                FetchDeliveryOrderRequest(
                    currentPage,
                    params.loadSize,
                    status,
                    start_date = startDate,
                    end_date = endDate,
                ),
            )

            val data = response.data

            LoadResult.Page(
                data = data.nodes.map {
                    DeliveryOrder(
                        id = it.id,
                        supplier_id = it.supplier_id,
                        user_id = it.user_id,
                        invoice_number = it.invoice_number,
                        date = it.date,
                        status = it.status,
                        total_price = it.total_price,
                        created_at = it.created_at,
                        updated_at = it.updated_at,
                        customer = Customer.fromResponse(it.customer),
                        items = arrayListOf(),
                        have_active_delivery = response.have_active_delivery,
                    )
                },
                prevKey = if (currentPage == 1) null else currentPage - 1,
                nextKey = if (data.nodes.isEmpty()) null else currentPage + 1
            )
        } catch (exception: IOException) {
            return LoadResult.Error(exception)
        } catch (exception: HttpException) {
            if (exception.code() == 404) {
                return LoadResult.Page(
                    data = listOf(),
                    prevKey = if (currentPage == 1) null else currentPage - 1,
                    nextKey = null,
                )
            } else {
                return LoadResult.Error(exception)
            }
        }
    }

    override fun getRefreshKey(state: PagingState<Int, DeliveryOrder>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }
}