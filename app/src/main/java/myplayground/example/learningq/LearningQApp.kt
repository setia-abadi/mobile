@file:OptIn(ExperimentalPermissionsApi::class)

package myplayground.example.learningq

import android.app.Application
import androidx.activity.ComponentActivity
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.DrawerState
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalDrawerSheet
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.isGranted
import com.google.accompanist.permissions.rememberMultiplePermissionsState
import kotlinx.coroutines.launch
import myplayground.example.learningq.di.Injection
import myplayground.example.learningq.ui.layout.Appbar
import myplayground.example.learningq.ui.layout.DrawerBodyStudent
import myplayground.example.learningq.ui.layout.DrawerHeader
import myplayground.example.learningq.ui.navigation.Screen
import myplayground.example.learningq.ui.screens.driver.active.DriverActiveScreen
import myplayground.example.learningq.ui.screens.driver.delivery_order_detail.DriverDeliveryOrderDetailScreen
import myplayground.example.learningq.ui.screens.driver.delivery_order_list.DriverDeliveryOrderListScreen
import myplayground.example.learningq.ui.screens.home.HomeScreen
import myplayground.example.learningq.ui.screens.landing.LandingScreen
import myplayground.example.learningq.ui.screens.setting.SettingScreen
import myplayground.example.learningq.ui.screens.sign_in.SignInScreen

@Composable
fun LearningQApp(
    modifier: Modifier = Modifier,
    navController: NavHostController = rememberNavController(),
    drawerState: DrawerState = rememberDrawerState(initialValue = DrawerValue.Closed),
) {
    val context = LocalContext.current
    val scope = rememberCoroutineScope()
    val navBackStackEntry = navController.currentBackStackEntryAsState()
    val currentRoute = navBackStackEntry.value?.destination?.route
    val hasNavPreviousBackStack = navController.previousBackStackEntry != null

    val authManager =
        Injection.provideAuthManager(LocalContext.current.applicationContext as Application)

    val globalManager =
        Injection.provideGlobalManager(LocalContext.current.applicationContext as Application)

    val haveToken = authManager.haveToken.collectAsState()
//    val currentRole = authManager.currentRole.collectAsState()
    val isLoading = authManager.isLoading.collectAsState()
    val user = authManager.user.collectAsState()

    val appbarTitle = globalManager.appbarTitle.collectAsState()
    val appbarTrailing = globalManager.appbarTrailing.collectAsState()

    val startDestination = if (haveToken.value) {
        if (isLoading.value) {
            Screen.AuthLoading.route
        } else {
            Screen.DriverDeliveryOrderList.route
        }
    } else {
        Screen.SignIn.route
    }

    // permission
    val permissionState = rememberMultiplePermissionsState(
        permissions = listOf(
            android.Manifest.permission.CAMERA,
            android.Manifest.permission.ACCESS_COARSE_LOCATION,
            android.Manifest.permission.ACCESS_FINE_LOCATION,
            // Add more permissions as needed
        )
    )


    LaunchedEffect(permissionState) {
        permissionState.launchMultiplePermissionRequest()
    }

    val showPermissionDialog by remember(permissionState) {
        derivedStateOf {
            permissionState.permissions.any { !it.status.isGranted }
        }
    }

    LocationTrackingScreen(context = context) {
        ModalNavigationDrawer(
            gesturesEnabled = drawerState.isOpen || (!hasNavPreviousBackStack && currentRoute != Screen.DriverActive.route),
            drawerState = drawerState,
            drawerContent = {
                if (haveToken.value) {
                    ModalDrawerSheet {
                        Column(
                            modifier = Modifier
                                .fillMaxSize()
                                .verticalScroll(rememberScrollState())
                        ) {
                            DrawerHeader(
                                user = user.value,
                            )
                            DrawerBodyStudent(
                                modifier = Modifier
                                    .weight(1F)
                                    .verticalScroll(rememberScrollState()),
                                navController = navController,
                                currentRoute = currentRoute ?: "",
                                authManager = authManager,
                                closeDrawer = {
                                    scope.launch {
                                        drawerState.close()
                                    }
                                },
                            )
                        }
                    }
                }
            },
        ) {
            Scaffold(
                topBar = {
                    if (!(currentRoute == Screen.SignIn.route || currentRoute == Screen.Landing.route)) {
                        Appbar(
                            navController = navController,
                            title = appbarTitle.value,
                            displayBackButton = hasNavPreviousBackStack,
                            onNavigationIconClick = {
                                scope.launch {
                                    drawerState.open()
                                }
                            },
                            trailing = appbarTrailing.value,
                        )
                    }
                },
            ) { innerPadding ->
                val containerModifier = Modifier
                    .background(MaterialTheme.colorScheme.background)
                    .padding(8.dp)

                NavHost(
                    navController = navController,
                    startDestination = startDestination,
                    modifier = Modifier.padding(innerPadding),
                ) {
                    composable(Screen.AuthLoading.route) { // empty
                    }

                    composable(Screen.Landing.route) {
                        globalManager.resetAppbar()
                        LandingScreen(
                            modifier = containerModifier, navController = navController
                        )
                    }
                    composable(Screen.SignIn.route) {
                        globalManager.resetAppbar()
                        SignInScreen(
//                        modifier = containerModifier,
                            navController = navController,
                        )
                    }

                    composable(Screen.Setting.route) {
                        globalManager.resetAppbar("Setting")
                        SettingScreen(
                            modifier = containerModifier,
                        )
                    }

                    composable(Screen.Home.route) {
                        globalManager.resetAppbar()
                        HomeScreen(
                            modifier = containerModifier,
                        )
                    }

                    composable(Screen.DriverActive.route) {
                        globalManager.resetAppbar(
                            trailing = {
                                Row(
                                    modifier = Modifier.fillMaxHeight(),
                                    verticalAlignment = Alignment.CenterVertically,
                                ) {
                                    Text(
                                        text = user.value?.name ?: "",
                                        color = MaterialTheme.colorScheme.onPrimary,
                                        style = MaterialTheme.typography.headlineSmall,
                                    )

                                    Spacer(modifier = Modifier.width(16.dp))

                                    Image(
                                        modifier = Modifier
                                            .width(40.dp)
                                            .clip(CircleShape),
                                        painter = painterResource(R.drawable.avatar_placeholder),
                                        contentDescription = "Profile Photo",
                                        contentScale = ContentScale.FillWidth,
                                    )
                                }
                            }
                        )
                        DriverActiveScreen(
                            modifier = containerModifier,
                        )
                    }

                    composable(Screen.DriverDeliveryOrderList.route) {
                        globalManager.resetAppbar("Delivery List")
                        DriverDeliveryOrderListScreen(
                            modifier = containerModifier,
                            navController = navController,
                        )
                    }

                    composable(
                        Screen.DriverDeliveryOrderDetail.route,
                        arguments = listOf(navArgument("id") { type = NavType.StringType })
                    ) {
                        globalManager.resetAppbar("Delivery Detail")
                        val deliveryOrderId = it.arguments?.getString("id") ?: ""

                        DriverDeliveryOrderDetailScreen(
                            modifier = containerModifier,
                            deliveryOrderId = deliveryOrderId,
                        )
                    }
                }
            }

            if (showPermissionDialog) {
                PermissionDeniedDialog(onDismiss = {
                    // Close the application
                    (context as? ComponentActivity)?.finish()
                })
            }
        }
    }
}

@Composable
fun PermissionDeniedDialog(onDismiss: () -> Unit) {
    AlertDialog(
        onDismissRequest = onDismiss,
        title = { Text("Permission Denied") },
        text = { Text("The app needs certain permissions to function properly.") },
        confirmButton = {
            Button(onClick = onDismiss) {
                Text("Close App")
            }
        }
    )
}