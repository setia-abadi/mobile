package myplayground.example.learningq

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.core.content.ContextCompat
import myplayground.example.learningq.ui.utils.LocationForegroundService
import myplayground.example.learningq.ui.utils.rememberServiceState

@Composable
fun LocationTrackingScreen(context: Context, content: @Composable () -> Unit) {
    val serviceIntent = remember { Intent(context, LocationForegroundService::class.java) }
    val isServiceRunning = rememberServiceState(context, serviceIntent)


    LaunchedEffect(null) {
        if (!isServiceRunning) {
            ContextCompat.startForegroundService(context, serviceIntent)
        }
    }

    content()
}