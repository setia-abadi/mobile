package myplayground.example.learningq.network

import myplayground.example.learningq.network.request.FetchDeliveryOrderRequest
import myplayground.example.learningq.network.request.LoginRequest
import myplayground.example.learningq.network.request.StudentCourseFetchRequest
import myplayground.example.learningq.network.request.UpdateDeliveryLocationRequest
import myplayground.example.learningq.network.response.DataResponse
import myplayground.example.learningq.network.response.DeliveryOrderResponse
import myplayground.example.learningq.network.response.DeliveryOrderResponseWrapper
import myplayground.example.learningq.network.response.FilterDriverDataResponse
import myplayground.example.learningq.network.response.LoginResponse
import myplayground.example.learningq.network.response.MeResponse
import myplayground.example.learningq.network.response.OSRMResponse
import myplayground.example.learningq.network.response.PaginationResponse
import myplayground.example.learningq.network.utils.WithCourses
import myplayground.example.learningq.network.utils.WithPagination
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.PATCH
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query


interface ApiService {

    @POST("auth/login-driver")
    suspend fun login(
        @Body body: LoginRequest,
    ): DataResponse<LoginResponse>

    @POST("/users/me")
    suspend fun userMe(): DataResponse<MeResponse>

    @POST("/delivery-orders/filter-driver")
    suspend fun fetchDeliveryOrder(
        @Body body: FetchDeliveryOrderRequest,
    ): FilterDriverDataResponse<PaginationResponse<DeliveryOrderResponse>>

    @PATCH("/delivery-orders/{id}/delivering")
    suspend fun deliveryOrderUpdateDelivering(
        @Path("id") deliveryOrderId: String,
    ): DataResponse<DeliveryOrderResponseWrapper>

    @PATCH("/delivery-orders/{id}/cancel")
    suspend fun deliveryOrderCancel(
        @Path("id") deliveryOrderId: String,
    ): DataResponse<DeliveryOrderResponseWrapper>

    @PATCH("/delivery-orders/{id}/completed")
    suspend fun deliveryOrderUpdateCompleted(
        @Path("id") deliveryOrderId: String,
    ): DataResponse<DeliveryOrderResponseWrapper>

    @PATCH("/delivery-orders/delivery-location")
    suspend fun updateDeliveryLocation(
        @Body body: UpdateDeliveryLocationRequest,
    )

    @GET("/delivery-orders/active-driver")
    suspend fun getActiveDeliveryOrder(): DataResponse<DeliveryOrderResponseWrapper?>

    @GET("/delivery-orders/{id}")
    suspend fun getDeliveryOrderById(
        @Path("id") deliveryOrderId: String,
    ): DataResponse<DeliveryOrderResponseWrapper>

    @GET("/routed-car/route/v1/driving/{start_longitude},{start_latitude};{destination_longitude},{destination_latitude}?overview=false&alternatives=true&steps=true")
    suspend fun osrm(
        @Path("start_longitude") startLongitude: Double,
        @Path("destination_longitude") destinationLongitude: Double,
        @Path("start_latitude") startLatitude: Double,
        @Path("destination_latitude") destinationLatitude: Double,
    ): OSRMResponse
}

/*
    Note for OSMR (Open Source Routing Machine)
    example api: https://routing.openstreetmap.de/routed-car/route/v1/driving/10.372810363769531,50.261852432375136;10.370664596557619,50.26827397182959?overview=false&alternatives=true&steps=true

    base api path: https://routing.openstreetmap.de

    API path: /routed-car/route/v1/driving/${start_longitude},${start_latitude};${destination_longitude},${destination_latitude}
    param: ?overview=false&alternatives=true&steps=true

    example response:

   {
    "code": "Ok",
    "routes": [
        {
            "legs": [
                {
                    "steps": [
                        {
                            "geometry": "owwqHc}h~@Th@",
                            "maneuver": {
                                "bearing_after": 230,
                                "bearing_before": 0,
                                "location": [
                                    10.372821,
                                    50.261843
                                ],
                                "type": "depart"
                            },
                            "mode": "driving",
                            "driving_side": "right",
                            "name": "",
                            "intersections": [
                                {
                                    "out": 0,
                                    "entry": [
                                        true
                                    ],
                                    "bearings": [
                                        230
                                    ],
                                    "location": [
                                        10.372821,
                                        50.261843
                                    ]
                                }
                            ],
                            "weight": 16.9,
                            "duration": 16.9,
                            "distance": 20
                        },
                        {
                            "geometry": "yvwqHy{h~@{AhBcA`A]Ro@^{@\\",
                            "maneuver": {
                                "bearing_after": 323,
                                "bearing_before": 229,
                                "location": [
                                    10.372605,
                                    50.261728
                                ],
                                "modifier": "right",
                                "type": "turn"
                            },
                            "mode": "driving",
                            "driving_side": "right",
                            "name": "",
                            "intersections": [
                                {
                                    "out": 2,
                                    "in": 0,
                                    "entry": [
                                        false,
                                        true,
                                        true
                                    ],
                                    "bearings": [
                                        45,
                                        150,
                                        330
                                    ],
                                    "location": [
                                        10.372605,
                                        50.261728
                                    ]
                                }
                            ],
                            "weight": 27.3,
                            "duration": 27.3,
                            "distance": 189.8
                        },
                        {
                            "geometry": "c`xqH{sh~@u@Tk@Nw@ToBj@SFWH_B^_Cr@uDjAu@HQA",
                            "maneuver": {
                                "bearing_after": 345,
                                "bearing_before": 341,
                                "location": [
                                    10.371339,
                                    50.263215
                                ],
                                "modifier": "straight",
                                "type": "new name"
                            },
                            "mode": "driving",
                            "driving_side": "right",
                            "name": "Unterhofer Straße",
                            "intersections": [
                                {
                                    "out": 1,
                                    "in": 0,
                                    "entry": [
                                        false,
                                        true
                                    ],
                                    "bearings": [
                                        165,
                                        345
                                    ],
                                    "location": [
                                        10.371339,
                                        50.263215
                                    ]
                                },
                                {
                                    "out": 2,
                                    "in": 0,
                                    "entry": [
                                        false,
                                        true,
                                        true
                                    ],
                                    "bearings": [
                                        165,
                                        270,
                                        345
                                    ],
                                    "location": [
                                        10.371039,
                                        50.26399
                                    ]
                                },
                                {
                                    "out": 2,
                                    "in": 0,
                                    "entry": [
                                        false,
                                        true,
                                        true
                                    ],
                                    "bearings": [
                                        165,
                                        270,
                                        345
                                    ],
                                    "location": [
                                        10.370734,
                                        50.26477
                                    ]
                                },
                                {
                                    "out": 0,
                                    "in": 2,
                                    "entry": [
                                        true,
                                        true,
                                        false
                                    ],
                                    "bearings": [
                                        0,
                                        45,
                                        165
                                    ],
                                    "location": [
                                        10.369925,
                                        50.266796
                                    ]
                                }
                            ],
                            "weight": 65.1,
                            "duration": 65.1,
                            "distance": 451.2
                        },
                        {
                            "geometry": "wxxqHyjh~@IA_DKMAm@MYE",
                            "maneuver": {
                                "bearing_after": 5,
                                "bearing_before": 355,
                                "location": [
                                    10.369887,
                                    50.267156
                                ],
                                "modifier": "straight",
                                "type": "new name"
                            },
                            "mode": "driving",
                            "driving_side": "right",
                            "name": "Schulstraße",
                            "intersections": [
                                {
                                    "out": 0,
                                    "in": 2,
                                    "entry": [
                                        true,
                                        true,
                                        false
                                    ],
                                    "bearings": [
                                        0,
                                        120,
                                        180
                                    ],
                                    "location": [
                                        10.369887,
                                        50.267156
                                    ]
                                },
                                {
                                    "out": 0,
                                    "in": 1,
                                    "entry": [
                                        true,
                                        false,
                                        true
                                    ],
                                    "bearings": [
                                        0,
                                        180,
                                        270
                                    ],
                                    "location": [
                                        10.369897,
                                        50.267213
                                    ]
                                },
                                {
                                    "out": 0,
                                    "in": 1,
                                    "entry": [
                                        true,
                                        false,
                                        true
                                    ],
                                    "bearings": [
                                        0,
                                        180,
                                        255
                                    ],
                                    "location": [
                                        10.369959,
                                        50.268006
                                    ]
                                }
                            ],
                            "weight": 21.4,
                            "duration": 21.4,
                            "distance": 143.3
                        },
                        {
                            "geometry": "w`yqH}kh~@Hc@TkA@E",
                            "maneuver": {
                                "bearing_after": 111,
                                "bearing_before": 8,
                                "location": [
                                    10.370069,
                                    50.268437
                                ],
                                "modifier": "right",
                                "type": "turn"
                            },
                            "mode": "driving",
                            "driving_side": "right",
                            "name": "",
                            "intersections": [
                                {
                                    "out": 1,
                                    "in": 2,
                                    "entry": [
                                        true,
                                        true,
                                        false
                                    ],
                                    "bearings": [
                                        15,
                                        105,
                                        195
                                    ],
                                    "location": [
                                        10.370069,
                                        50.268437
                                    ]
                                }
                            ],
                            "weight": 11,
                            "duration": 11,
                            "distance": 46.2
                        },
                        {
                            "geometry": "u_yqHsoh~@",
                            "maneuver": {
                                "bearing_after": 0,
                                "bearing_before": 120,
                                "location": [
                                    10.37066,
                                    50.268268
                                ],
                                "type": "arrive"
                            },
                            "mode": "driving",
                            "driving_side": "right",
                            "name": "",
                            "intersections": [
                                {
                                    "in": 0,
                                    "entry": [
                                        true
                                    ],
                                    "bearings": [
                                        300
                                    ],
                                    "location": [
                                        10.37066,
                                        50.268268
                                    ]
                                }
                            ],
                            "weight": 0,
                            "duration": 0,
                            "distance": 0
                        }
                    ],
                    "summary": "Unterhofer Straße, Schulstraße",
                    "weight": 141.7,
                    "duration": 141.7,
                    "distance": 850.5
                }
            ],
            "weight_name": "routability",
            "weight": 141.7,
            "duration": 141.7,
            "distance": 850.5
        }
    ],
    "waypoints": [
        {
            "hint": "OX3PgHeS9oSQAAAA7gQAAAAAAACTCQAAHiWgQahML0MAAAAA5yiqQ5AAAADuBAAAAAAAAJMJAAClBQEA1UaeAFPv_gLKRp4AXO_-AgAALwM7754o",
            "distance": 1.271654031,
            "name": "",
            "location": [
                10.372821,
                50.261843
            ]
        },
        {
            "hint": "9IUQjPWFEIwGAAAAJwAAAGgAAAAQAAAApxotQN4bgEFl1i1COx_XQAYAAAAnAAAAaAAAABAAAAClBQEAZD6eAGwI_wJpPp4Acgj_AgIAbwc7754o",
            "distance": 0.756619577,
            "name": "",
            "location": [
                10.37066,
                50.268268
            ]
        }
    ]
}
 */