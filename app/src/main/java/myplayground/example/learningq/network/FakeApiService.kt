package myplayground.example.learningq.network

import kotlinx.coroutines.delay
import myplayground.example.learningq.local_storage.LocalStorageManager
import myplayground.example.learningq.model.Role
import myplayground.example.learningq.model.Token
import myplayground.example.learningq.model.User
import myplayground.example.learningq.network.request.FetchDeliveryOrderRequest
import myplayground.example.learningq.network.request.LoginRequest
import myplayground.example.learningq.network.request.UpdateDeliveryLocationRequest
import myplayground.example.learningq.network.response.DataResponse
import myplayground.example.learningq.network.response.DeliveryOrderResponse
import myplayground.example.learningq.network.response.DeliveryOrderResponseWrapper
import myplayground.example.learningq.network.response.FilterDriverDataResponse
import myplayground.example.learningq.network.response.LoginResponse
import myplayground.example.learningq.network.response.MeResponse
import myplayground.example.learningq.network.response.OSRMResponse
import myplayground.example.learningq.network.response.PaginationResponse
import okhttp3.ResponseBody.Companion.toResponseBody
import retrofit2.HttpException
import retrofit2.Response
import retrofit2.http.Header

class FakeApiService(val localStorageManager: LocalStorageManager) : ApiService {
    override suspend fun login(body: LoginRequest): DataResponse<LoginResponse> {
        delay(1500)
//        for (currentUser in listUser) {
//            if (body.username.equals(
//                    currentUser.username, ignoreCase = true
//                ) && body.password == currentUser.password
//            ) {
        return DataResponse<LoginResponse>(
            data = LoginResponse(
                accessToken = "token Driver",
                accessTokenExpiredAt = "",
                tokenType = "Bearer",
            )
        )
//            }
//        }

        throw HttpException(
            Response.error<Token?>(400, "Invalid User/Password".toResponseBody())
        )
    }

    override suspend fun userMe(): DataResponse<MeResponse> {
        throw Exception("Not handled")
//        delay(500)
//
//        val token = localStorageManager.getUserToken()
//
//
//        if (token.length <= "token ".length) {
//            return MeResponse(
//                superAdminUser,
//            )
//        }
//
//        return when (token.substring("token ".length)) {
//            "student" -> {
//                MeResponse(
//                    superAdminUser,
//                )
//            }
//
//            "teacher" -> {
//                MeResponse(
//                    teacherUser,
//                )
//
//            }
//
//            "admin" -> {
//                MeResponse(
//                    adminUser,
//                )
//
//            }
//
//            "parent" -> {
//                MeResponse(
//                    parentUser,
//                )
//
//            }
//
//            else -> MeResponse(
//                superAdminUser,
//            )
//        }
    }

    override suspend fun fetchDeliveryOrder(body: FetchDeliveryOrderRequest): FilterDriverDataResponse<PaginationResponse<DeliveryOrderResponse>> {
        TODO("Not yet implemented")
    }

    override suspend fun deliveryOrderUpdateDelivering(deliveryOrderId: String): DataResponse<DeliveryOrderResponseWrapper> {
        TODO("Not yet implemented")
    }

    override suspend fun deliveryOrderCancel(deliveryOrderId: String): DataResponse<DeliveryOrderResponseWrapper> {
        TODO("Not yet implemented")
    }

    override suspend fun deliveryOrderUpdateCompleted(deliveryOrderId: String): DataResponse<DeliveryOrderResponseWrapper> {
        TODO("Not yet implemented")
    }

    override suspend fun updateDeliveryLocation(
        body: UpdateDeliveryLocationRequest
    ) {
        TODO("Not yet implemented")
    }

    override suspend fun getActiveDeliveryOrder(): DataResponse<DeliveryOrderResponseWrapper?> {
        TODO("Not yet implemented")
    }

    override suspend fun getDeliveryOrderById(deliveryOrderId: String): DataResponse<DeliveryOrderResponseWrapper> {
        TODO("Not yet implemented")
    }

    override suspend fun osrm(
        startLongitude: Double,
        destinationLongitude: Double,
        startLatitude: Double,
        destinationLatitude: Double
    ): OSRMResponse {
        TODO("Not yet implemented")
    }


    val superAdminUser = User(
        id = "1",
        username = "super.admin.one",
        name = "Super Admin",
//            image_url = "https://miro.medium.com/v2/resize:fill:110:110/1*x1I-A7aVdqWFelvJakKWBg.jpeg",
    )


    val listUser = listOf(
        superAdminUser,
    )


    companion object {
        @Volatile
        private var instance: FakeApiService? = null
        fun getInstance(
            localStorageManager: LocalStorageManager,
        ): FakeApiService = instance ?: synchronized(this) {
            FakeApiService(
                localStorageManager = localStorageManager,
            ).apply {
                instance = this
            }
        }
    }
}
