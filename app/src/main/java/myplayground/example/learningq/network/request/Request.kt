package myplayground.example.learningq.network.request

import java.io.Serializable

data class LoginRequest(
    val username: String,
    val password: String,
) : Serializable


data class FetchDeliveryOrderRequest(
    val page: Int?,
    val limit: Int?,
    val status: String?,
    val start_date: String?,
    val end_date: String?,
) : Serializable

data class UpdateDeliveryLocationRequest(
    val latitude: Double,
    val longitude: Double,
    val bearing: Double
) : Serializable


