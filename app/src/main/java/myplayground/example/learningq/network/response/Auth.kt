package myplayground.example.learningq.network.response

import com.google.gson.annotations.SerializedName

data class LoginResponse (
    @SerializedName("access_token")
    val accessToken: String,
    @SerializedName("access_token_expired_at")
    val accessTokenExpiredAt : String,
    @SerializedName("token_type")
    val tokenType: String,
)