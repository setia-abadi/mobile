package myplayground.example.learningq.network.response

import com.google.gson.annotations.SerializedName
import myplayground.example.learningq.model.User

data class DataResponse<T>(
    val data: T,
)

data class FilterDriverDataResponse<T>(
    val data: T,
    val have_active_delivery: Boolean,
)

data class PaginationResponse<T>(
    val total: Int,
    val page: Int?,
    val limit: Int?,
    val nodes: ArrayList<T>,
)

data class MeResponse(
    val user: User,
)

data class DeliveryOrderResponseWrapper(
    val delivery_order: DeliveryOrderResponse?
)

data class DeliveryOrderResponse(
    val id: String,
    val supplier_id: String,
    val user_id: String,
    val invoice_number: String,
    val date: String,
    val status: String,
    val total_price: Double,
    val created_at: String,
    val updated_at: String,
    val customer: CustomerResponse?,
    val items: ArrayList<DeliveryOrderItemResponse>?,
    val images: ArrayList<DeliveryOrderImageResponse>?,
)

data class CustomerResponse(
    val id: String,
    val name: String,
    val email: String,
    val address: String,
    val latitude: Double,
    val longitude: Double,
    val phone: String,
    val created_at: String,
    val updated_at: String,
)

data class DeliveryOrderItemResponse(
    val id: String,
    val delivery_order_id: String,
    val product_unit_id: String,
    val qty: Int,
    val price_per_unit: Double,
    val price_total: Double,
    val discount_per_unit: Double,
    val created_at: String,
    val updated_at: String,
    val product_unit: ProductUnitResponse?,
)

data class DeliveryOrderImageResponse(
    val id: String,
    val delivery_order_id: String,
    val file_id: String,
    val description: String?,
    val created_at: String,
    val updated_at: String,

    val file: FileResponse?,
)

data class ProductUnitResponse(
    val id: String,
    val to_unit_id: String,
    val unit_id: String,
    val product_id: String,
    val scale: Int,
    val scale_to_base: Int,
    val created_at: String,
    val updated_at: String,
    val product: ProductResponse?,
    val unit: UnitResponse?,
)

data class ProductResponse(
    val id: String,
    val name: String,
    val description: String?,
    val price: Double,
    val is_active: Boolean,
    val created_at: String,
    val updated_at: String,
    val image_file: FileResponse?,
)

data class UnitResponse(
    val id: String,
    val name: String,
    val description: String?,
    val created_at: String,
    val updated_at: String,
)

data class FileResponse(
    val id: String,
    val name: String,
    val path: String,
    val link: String?,
    val created_at: String,
    val updated_at: String,
)

// START::OSRM
data class OSRMResponse(
    @SerializedName("code") var code: String? = null,
    @SerializedName("routes") var routes: ArrayList<Routes> = arrayListOf(),
    @SerializedName("waypoints") var waypoints: ArrayList<Waypoints> = arrayListOf()
)

data class Intersections(
    @SerializedName("out") var out: Int? = null,
    @SerializedName("entry") var entry: ArrayList<Boolean> = arrayListOf(),
    @SerializedName("bearings") var bearings: ArrayList<Int> = arrayListOf(),
    @SerializedName("location") var location: ArrayList<Double> = arrayListOf()
)

data class Legs(
    @SerializedName("steps") var steps: ArrayList<Steps> = arrayListOf(),
    @SerializedName("summary") var summary: String? = null,
    @SerializedName("weight") var weight: Double? = null,
    @SerializedName("duration") var duration: Double? = null,
    @SerializedName("distance") var distance: Double? = null
)

data class Maneuver(
    @SerializedName("bearing_after") var bearingAfter: Int? = null,
    @SerializedName("bearing_before") var bearingBefore: Int? = null,
    @SerializedName("location") var location: ArrayList<Double> = arrayListOf(),
    @SerializedName("type") var type: String? = null
)

data class Routes(
    @SerializedName("legs") var legs: ArrayList<Legs> = arrayListOf(),
    @SerializedName("weight_name") var weightName: String? = null,
    @SerializedName("weight") var weight: Double? = null,
    @SerializedName("duration") var duration: Double? = null,
    @SerializedName("distance") var distance: Double? = null
)

data class Steps(
    @SerializedName("geometry") var geometry: String? = null,
    @SerializedName("maneuver") var maneuver: Maneuver? = Maneuver(),
    @SerializedName("mode") var mode: String? = null,
    @SerializedName("driving_side") var drivingSide: String? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("intersections") var intersections: ArrayList<Intersections> = arrayListOf(),
    @SerializedName("weight") var weight: Double? = null,
    @SerializedName("duration") var duration: Double? = null,
    @SerializedName("distance") var distance: Double? = null
)

data class Waypoints(
    @SerializedName("hint") var hint: String? = null,
    @SerializedName("distance") var distance: Double? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("location") var location: ArrayList<Double> = arrayListOf()
)
// END::OSRM