package myplayground.example.learningq.utils

import androidx.compose.runtime.Composable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import myplayground.example.learningq.di.Injection
import myplayground.example.learningq.local_storage.LocalStorageManager
import myplayground.example.learningq.model.Role
import myplayground.example.learningq.model.User
import myplayground.example.learningq.repository.Repository


class GlobalManager(
) {
    private val _appbarTitle = MutableStateFlow("")
    private val _appbarTrailing: MutableStateFlow<@Composable () -> Unit> = MutableStateFlow {}

    val appbarTitle: StateFlow<String> = _appbarTitle
    val appbarTrailing: StateFlow<@Composable () -> Unit> = _appbarTrailing

    private val coroutineScope = CoroutineScope(Dispatchers.Default)

    fun setAppbarTitle(title: String) {
        coroutineScope.launch {
            _appbarTitle.emit(title)
        }
    }

    fun setAppbarTrailing(trailing: @Composable () -> Unit) {
        coroutineScope.launch {
            _appbarTrailing.emit(trailing)
        }
    }


    fun resetAppbar(title: String = "", trailing: @Composable () -> Unit = {}) {
        setAppbarTitle(title)
        setAppbarTrailing(trailing)
    }

    companion object {
        @Volatile
        private var instance: GlobalManager? = null

        fun getInstance(): GlobalManager {
            return instance ?: synchronized(this) {
                instance ?: GlobalManager().also {
                    instance = it
                }
            }
        }
    }
}