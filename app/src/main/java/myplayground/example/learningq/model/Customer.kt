package myplayground.example.learningq.model

import myplayground.example.learningq.network.response.CustomerResponse

data class Customer(
    val id: String,
    val name: String,
    val email: String,
    val address: String,
    val latitude: Double,
    val longitude: Double,
    val phone: String,
    val created_at: String,
    val updated_at: String,
) {
    companion object {
        fun fromResponse(resp: CustomerResponse?): Customer? {
            if (resp == null) return null

            return Customer(
                id = resp.id,
                name = resp.name,
                email = resp.email,
                address = resp.address,
                latitude = resp.latitude,
                longitude = resp.longitude,
                phone = resp.phone,
                created_at = resp.created_at,
                updated_at = resp.updated_at,
            )
        }
    }
}
