package myplayground.example.learningq.model

sealed class Role {
    object Driver: Role()
    object None: Role()
//    object Student : Role()
//    object Teacher : Role()
//    object Admin : Role()
//    object Parent : Role()

    override fun toString(): String {
        return when (this) {
            is Driver -> "Driver"
            is None -> "None"
//            is Student -> "student"
//            is Teacher -> "teacher"
//            is Admin -> "admin"
//            is Parent -> "parent"
        }
    }

    companion object {
        fun list() = listOf(
            Driver,
            None,
//            Student,
//            Teacher,
//            Admin,
//            Parent,
        )

        fun parseString(str: String): Role {
            return when (str) {
                "Driver" -> Driver
//                "student" -> Student
//                "teacher" -> Teacher
//                "admin" -> Admin
//                "parent" -> Parent

                else -> None
            }
        }
    }
}
