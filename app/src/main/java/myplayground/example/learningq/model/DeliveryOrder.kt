package myplayground.example.learningq.model

import myplayground.example.learningq.network.response.DeliveryOrderImageResponse
import myplayground.example.learningq.network.response.DeliveryOrderItemResponse
import myplayground.example.learningq.network.response.DeliveryOrderResponse

data class DeliveryOrder(
    val id: String,
    val supplier_id: String,
    val user_id: String,
    val invoice_number: String,
    val date: String,
    val status: String,
    val total_price: Double,
    val created_at: String,
    val updated_at: String,
    val customer: Customer?,
    val items: ArrayList<DeliveryOrderItemResponse>? = arrayListOf(),
    val images: ArrayList<DeliveryOrderImageResponse>? = arrayListOf(),
    val have_active_delivery: Boolean = false,
) {


    fun statusToIndo(): String {
        return when (status) {
            "CANCELED" -> "Dibatalkan"
            "ONGOING" -> "Menunggu"
            "DELIVERING" -> "Dalam Pengiriman"
            "COMPLETED" -> "Selesai"
            "RETURNED" -> "Dikembalikan"
            else -> ""
        }
    }

    companion object {
        fun fromResponse(resp: DeliveryOrderResponse): DeliveryOrder {
            return DeliveryOrder(
                id = resp.id,
                supplier_id = resp.supplier_id,
                user_id = resp.user_id,
                invoice_number = resp.invoice_number,
                date = resp.date,
                status = resp.status,
                total_price = resp.total_price,
                created_at = resp.created_at,
                updated_at = resp.updated_at,
                customer = Customer.fromResponse(resp.customer),
                items = resp.items,
                images = resp.images,
            )
        }
    }
}
