package myplayground.example.learningq.model

data class CustomLatLng(
    val lat: Double,
    val lon: Double,
    val bear: Double,
)
