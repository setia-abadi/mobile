package myplayground.example.learningq.ui.screens.driver.delivery_order_detail

import android.annotation.SuppressLint
import android.content.Context
import android.location.LocationManager
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import myplayground.example.learningq.di.Injection
import myplayground.example.learningq.local_storage.LocalStorageManager
import myplayground.example.learningq.model.Class
import myplayground.example.learningq.model.DeliveryOrder
import myplayground.example.learningq.repository.Repository

class DriverDeliveryOrderDetailViewModel(
    private val context: Context,
    private val repository: Repository,
    private val localStorageManager: LocalStorageManager,
) :
    ViewModel() {
    private val _classState: MutableStateFlow<PagingData<Class>> =
        MutableStateFlow(PagingData.empty())
    private val _deliveryOrder: MutableState<DeliveryOrder?> = mutableStateOf(null)
    private val locationManager =
        context.getSystemService(Context.LOCATION_SERVICE) as LocationManager

    val deliveryOrder: State<DeliveryOrder?> = _deliveryOrder

    init {
        onEvent(DriverDeliveryOrderDetailEvent.Init)
    }

    @SuppressLint("MissingPermission")
    fun onEvent(event: DriverDeliveryOrderDetailEvent) {
        viewModelScope.launch {
            when (event) {
                is DriverDeliveryOrderDetailEvent.Init -> {
                }

                is DriverDeliveryOrderDetailEvent.GetDeliveryOrder -> {
                    _deliveryOrder.value = repository.getDeliveryOrderById(
                        event.deliveryOrderId,
                        Injection.provideApiService(localStorageManager),
                    )
                }
            }
        }
    }
}


