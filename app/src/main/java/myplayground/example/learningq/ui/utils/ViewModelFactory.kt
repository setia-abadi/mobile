package myplayground.example.learningq.ui.utils

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import myplayground.example.learningq.ThemeViewModel
import myplayground.example.learningq.di.Injection
import myplayground.example.learningq.local_storage.LocalStorageManager
import myplayground.example.learningq.repository.FakeRepository
import myplayground.example.learningq.repository.Repository
import myplayground.example.learningq.ui.screens.driver.active.DriverActiveViewModel
import myplayground.example.learningq.ui.screens.driver.delivery_order_detail.DriverDeliveryOrderDetailViewModel
import myplayground.example.learningq.ui.screens.driver.delivery_order_list.DriverDeliveryOrderListViewModel
import myplayground.example.learningq.ui.screens.home.HomeViewModel
import myplayground.example.learningq.ui.screens.sign_in.SignInViewModel

@Suppress("UNCHECKED_CAST")
class ViewModelFactory(
    private val application: Application,
    private val repository: Repository,
    private val localStorageManager: LocalStorageManager,
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        val fakeRepository = FakeRepository.getInstance(application.applicationContext)

        val authManager = Injection.provideAuthManager(application.applicationContext)
//        val fusedLocationClient 

        if (modelClass.isAssignableFrom(HomeViewModel::class.java)) {
            return HomeViewModel(repository, authManager) as T
        } else if (modelClass.isAssignableFrom(ThemeViewModel::class.java)) {
            return ThemeViewModel.getInstance(localStorageManager) as T
        } else if (modelClass.isAssignableFrom(SignInViewModel::class.java)) {
            return SignInViewModel(
                application.applicationContext,
                repository,
                localStorageManager
            ) as T
        } else if (modelClass.isAssignableFrom(DriverActiveViewModel::class.java)) {
            return DriverActiveViewModel(
                application.applicationContext,
                repository,
                localStorageManager
            ) as T
        } else if (modelClass.isAssignableFrom(DriverDeliveryOrderListViewModel::class.java)) {
            return DriverDeliveryOrderListViewModel(repository, localStorageManager) as T
        } else if (modelClass.isAssignableFrom(DriverDeliveryOrderDetailViewModel::class.java)) {
            return DriverDeliveryOrderDetailViewModel(
                application.applicationContext,
                repository,
                localStorageManager
            ) as T
        }

        throw IllegalArgumentException("Unknown ViewModel class: " + modelClass.name)
    }
}