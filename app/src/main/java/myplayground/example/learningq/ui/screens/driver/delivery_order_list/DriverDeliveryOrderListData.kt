package myplayground.example.learningq.ui.screens.driver.delivery_order_list

sealed class DriverDeliveryOrderListEvent {
    object Init : DriverDeliveryOrderListEvent()
    data class Delivering(val id: String) : DriverDeliveryOrderListEvent()
    data class Cancel(val id: String) : DriverDeliveryOrderListEvent()
    data class Completed(val id: String) : DriverDeliveryOrderListEvent()
    data class SetFilter(val status: String?, val startDate: String?, val endDate: String?) :
        DriverDeliveryOrderListEvent()

}
