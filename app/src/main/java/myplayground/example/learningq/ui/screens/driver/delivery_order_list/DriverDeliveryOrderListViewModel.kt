package myplayground.example.learningq.ui.screens.driver.delivery_order_list

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.map
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.launch
import myplayground.example.learningq.di.Injection
import myplayground.example.learningq.local_storage.LocalStorageManager
import myplayground.example.learningq.model.DeliveryOrder
import myplayground.example.learningq.repository.Repository
import retrofit2.HttpException

class DriverDeliveryOrderListViewModel(
    private val repository: Repository,
    private val localStorageManager: LocalStorageManager,
) :
    ViewModel() {
    private val _paginationState: MutableStateFlow<PagingData<DeliveryOrder>> =
        MutableStateFlow(PagingData.empty())
    private val _isLoading: MutableState<Boolean> = mutableStateOf(false)
    private val _filteredStatus: MutableState<String?> = mutableStateOf(null)
    private val _startDate: MutableState<String?> = mutableStateOf(null)
    private val _endDate: MutableState<String?> = mutableStateOf(null)


    val paginationState: StateFlow<PagingData<DeliveryOrder>> = _paginationState
    val isLoading: State<Boolean> = _isLoading
    val filteredStatus: State<String?> = _filteredStatus
    val startDate: State<String?> = _startDate
    val endDate: State<String?> = _endDate

    init {
        onEvent(DriverDeliveryOrderListEvent.Init)
    }

    fun onEvent(event: DriverDeliveryOrderListEvent) {
        viewModelScope.launch {
            when (event) {
                is DriverDeliveryOrderListEvent.Init -> {
                    fetchDeliveryOrderPaging()
                }

                is DriverDeliveryOrderListEvent.Delivering -> {
                    _isLoading.value = true
                    try {
                        val deliveryOrder = repository.deliveryOrderUpdateDelivering(
                            event.id,
                            Injection.provideApiService(localStorageManager),
                        )
                        _isLoading.value = false
                        fetchDeliveryOrderPaging()
//                        updateDeliveryOrderPaginationState(deliveryOrder)
                    } catch (e: HttpException) {
//                        Log.i("ERRORRRRRRRRR ", e.response()?.errorBody()?.string() ?: "")
                    } finally {
                        _isLoading.value = false
                    }
                }

                is DriverDeliveryOrderListEvent.Cancel -> {
                    _isLoading.value = true
                    try {
                        val deliveryOrder = repository.deliveryOrderCancel(
                            event.id,
                            Injection.provideApiService(localStorageManager),
                        )
                        _isLoading.value = false
                        fetchDeliveryOrderPaging()
//                        updateDeliveryOrderPaginationState(deliveryOrder)
                    } catch (e: HttpException) {
//                        Log.i("ERRORRRRRRRRR ", e.response()?.errorBody()?.string() ?: "")
                    } finally {
                        _isLoading.value = false
                    }
                }

                is DriverDeliveryOrderListEvent.Completed -> {
                    _isLoading.value = true
                    try {
                        val deliveryOrder = repository.deliveryOrderUpdateCompleted(
                            event.id,
                            Injection.provideApiService(localStorageManager),
                        )
                        _isLoading.value = false
                        fetchDeliveryOrderPaging()
//                        updateDeliveryOrderPaginationState(deliveryOrder)
                    } catch (e: HttpException) {

                    } finally {
                        _isLoading.value = false
                    }
                }

                is DriverDeliveryOrderListEvent.SetFilter -> {
                    _filteredStatus.value = event.status
                    _startDate.value = event.startDate
                    _endDate.value = event.endDate

                    fetchDeliveryOrderPaging()
                }
            }
        }
    }

    private suspend fun fetchDeliveryOrderPaging() {
        repository.fetchDeliveryOrderPaging(
            Injection.provideApiService(localStorageManager = localStorageManager),
            _filteredStatus.value,
            _startDate.value,
            _endDate.value,
        )
            .distinctUntilChanged()
            .cachedIn(viewModelScope)
            .collect {
                _paginationState.value = it
            }
    }

    private fun updateDeliveryOrderPaginationState(newDeliveryOrder: DeliveryOrder) {
        _paginationState.value = _paginationState.value.map { deliveryOrder ->
            if (deliveryOrder.id == newDeliveryOrder.id) {
                // Update the delivery order status here
                newDeliveryOrder
            } else {
                deliveryOrder
            }
        }
    }
}