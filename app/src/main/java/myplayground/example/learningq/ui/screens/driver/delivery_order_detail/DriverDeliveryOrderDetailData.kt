package myplayground.example.learningq.ui.screens.driver.delivery_order_detail

sealed class DriverDeliveryOrderDetailEvent {
    object Init : DriverDeliveryOrderDetailEvent()
    data class GetDeliveryOrder(val deliveryOrderId: String) : DriverDeliveryOrderDetailEvent()
}