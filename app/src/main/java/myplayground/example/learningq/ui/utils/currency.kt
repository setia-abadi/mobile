package myplayground.example.learningq.ui.utils

import java.text.NumberFormat
import java.util.Locale

fun formatCurrency(amount: Double): String {
    val localeID = Locale("id", "ID")
    val currencyFormatter = NumberFormat.getCurrencyInstance(localeID)

    return currencyFormatter.format(amount)
}