package myplayground.example.learningq.ui.screens.driver.active

import android.app.Application
import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import myplayground.example.learningq.di.Injection
import myplayground.example.learningq.local_storage.DatastoreSettings
import myplayground.example.learningq.local_storage.dataStore
import myplayground.example.learningq.ui.theme.LearningQTheme
import myplayground.example.learningq.ui.utils.ViewModelFactory
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.google.maps.android.ktx.awaitMap
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import myplayground.example.learningq.R
import myplayground.example.learningq.model.CustomLatLng
import myplayground.example.learningq.model.DeliveryOrder
import myplayground.example.learningq.ui.utils.rememberMapViewWithLifecycle

@Composable
fun DriverActiveScreen(
    modifier: Modifier = Modifier,
    vm: DriverActiveViewModel = viewModel(
        factory = ViewModelFactory(
            LocalContext.current.applicationContext as Application,
            Injection.provideRepository(LocalContext.current),
            DatastoreSettings.getInstance(LocalContext.current.dataStore),
        )
    ),
) {
//    val classesPagingItem = vm.classState.collectAsLazyPagingItems()
    val currentLocation by vm.currentLocation
    val deliveryOrder by vm.deliveryOrder
    val lines by vm.currentLines

    LaunchedEffect(currentLocation, deliveryOrder) {
        vm.onEvent(DriverActiveEvent.OSRM)
    }

    DriverActiveContent(
        modifier = modifier,
        currentLocation = currentLocation,
        deliveryOrder = deliveryOrder,
        lines = lines,
        vm::onEvent,
        vm::bitmapDescriptorFromVector,
//        classesPagingItem = classesPagingItem
    )
}


//@Composable
//fun MapScreen() {
//    val viewModel = viewModel<MapViewModel>()
//    val mapView = rememberMapView()
//
//    MapViewComposable(
//        modifier = Modifier.fillMaxSize(),
//        mapView = mapView
//    ) { googleMap ->
//        googleMap.addMarker(
//            MarkerOptions()
//                .position(LatLng(37.7749, -122.4194))
//                .title("San Francisco")
//        )
//    }
//}

@Composable
fun DriverActiveContent(
    modifier: Modifier = Modifier,
    currentLocation: CustomLatLng? = null,
    deliveryOrder: DeliveryOrder? = null,
    lines: ArrayList<LatLng>? = null,
    onEvent: (event: DriverActiveEvent) -> Unit = {},
    bitmapDescriptorFromVector: (
        vectorResId: Int
    ) -> BitmapDescriptor? = { _ ->
        null
    },
) {
    val mapView = rememberMapViewWithLifecycle()

    Box(
        modifier = Modifier
            .fillMaxHeight()
            .fillMaxWidth()
            .background(Color.White)
    ) {
        AndroidView({ mapView }) { mapView ->
            CoroutineScope(Dispatchers.Main).launch {
                val map = mapView.awaitMap()

                map.uiSettings.isZoomControlsEnabled = true
                map.clear()


                val boundsBuilder = LatLngBounds.Builder()
                var pointsIncluded = false

                currentLocation?.let { it ->
                    val pickUp = LatLng(it.lat, it.lon)


                    // adjust icon scale level
                    val icon = bitmapDescriptorFromVector(
                        R.drawable.truck,
                    )

                    val markerOptions = MarkerOptions().title("Pick Up").position(pickUp)
                        .icon(icon)
                        .rotation(it.bear.toFloat())


                    map.addMarker(markerOptions)

                    boundsBuilder.include(pickUp)
                    pointsIncluded = true
                }

                deliveryOrder?.customer?.let { it ->
                    map.addMarker(
                        MarkerOptions().title("Destination")
                            .position(LatLng(it.latitude, it.longitude)),
                    )

                    boundsBuilder.include(LatLng(it.latitude, it.longitude))
                    pointsIncluded = true
                }

                lines?.let { it ->
                    var i = 0

                    val polylineOptions =
                        PolylineOptions()
                            .color(android.graphics.Color.parseColor("#0F53FF"))
                            .width(20.0F)
                    while (i < it.size - 1) {
                        polylineOptions.add(it[i], it[i + 1])

                        boundsBuilder.include(it[i])
                        boundsBuilder.include(it[i + 1])
                        pointsIncluded = true

                        i++
                    }

                    map.addPolyline(polylineOptions)
                }

                if (pointsIncluded) {
                    val bounds = boundsBuilder.build()
                    val padding = 100 // Adjust padding as needed
                    val cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding)
                    map.animateCamera(cameraUpdate)
                }
            }
        }

        // Overlay layer for additional information
        Card(
            modifier = Modifier.fillMaxWidth(),
            colors = CardDefaults.cardColors(
                containerColor = MaterialTheme.colorScheme.tertiary,
            ),
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(Color.White)
                    .padding(16.dp) // Add padding as needed
            ) {
                // Display additional information here based on currentLocation, deliveryOrder, or lines
                deliveryOrder?.customer?.let { customer ->
                    Text(
                        text = deliveryOrder.invoice_number,
                        style = MaterialTheme.typography.titleSmall,
                        color = MaterialTheme.colorScheme.onTertiary,
                    )
                    Spacer(modifier = Modifier.height(5.dp))
                    Text(
                        text = customer.name,
                        style = MaterialTheme.typography.titleSmall,
                        color = MaterialTheme.colorScheme.onTertiary,
                    )
                    Spacer(modifier = Modifier.height(5.dp))
                    Text(
                        text = customer.address,
                        style = MaterialTheme.typography.bodySmall,
                        color = MaterialTheme.colorScheme.onTertiary,
                    )
                    Spacer(modifier = Modifier.height(5.dp))
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        Arrangement.SpaceBetween,
                    ) {
                        Text(
                            text = "${deliveryOrder.items?.size ?: "0"} Barang",
                            style = MaterialTheme.typography.displaySmall,
                            color = MaterialTheme.colorScheme.onTertiary,
                        )
                        Text(
                            text = deliveryOrder.date,
                            style = MaterialTheme.typography.displaySmall,
                            color = MaterialTheme.colorScheme.onTertiary,
                        )
                    }
                    Spacer(modifier = Modifier.height(5.dp))
                }
                // Add more Text composables or other UI components as needed
            }
        }
    }

}


//
//@Composable
//fun MapViewComposable(
//    modifier: Modifier = Modifier,
//    mapView: MapView,
//    onMapReady: (GoogleMap) -> Unit
//) {
//    AndroidView(
//        factory = { mapView },
//        modifier = modifier,
//        update = { mapView ->
//            mapView.onCreate(null)
//            mapView.getMapAsync { googleMap ->
//                onMapReady(googleMap)
//            }
//        }
//    )
//}


@Preview(showBackground = true, device = Devices.PIXEL_4)
@Preview(showBackground = true, device = Devices.PIXEL_4, uiMode = UI_MODE_NIGHT_YES)
@Composable
fun DriverActiveContentPreview() {
    LearningQTheme {
        DriverActiveContent()
    }
}

