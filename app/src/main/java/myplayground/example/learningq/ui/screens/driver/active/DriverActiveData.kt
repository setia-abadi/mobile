package myplayground.example.learningq.ui.screens.driver.active

sealed class DriverActiveEvent {
    object Init : DriverActiveEvent()
    object OSRM : DriverActiveEvent()
}