package myplayground.example.learningq.ui.navigation

sealed class Screen(val route: String) {
    object Home : Screen("home")
    object SignIn : Screen("signin")
    object Landing : Screen("landing")
    object Setting : Screen("setting")

    object AuthLoading : Screen("authloading")


    object DriverActive : Screen("driveractive")
    object DriverDeliveryOrderList : Screen("driverdeliveryorderlist")
    object DriverDeliveryOrderDetail : Screen("driverdeliveryorderlist/{id}") {
        fun createRoute(deliveryOrderId: String) = "driverdeliveryorderlist/$deliveryOrderId"
    }
}
