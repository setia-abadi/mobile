package myplayground.example.learningq.ui.screens.driver.delivery_order_list

import android.app.Application
import android.content.res.Configuration.UI_MODE_NIGHT_YES
import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.FilterAlt
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.material3.ExposedDropdownMenuDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import androidx.paging.LoadState
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.collectAsLazyPagingItems
import myplayground.example.learningq.R
import myplayground.example.learningq.di.Injection
import myplayground.example.learningq.local_storage.DatastoreSettings
import myplayground.example.learningq.local_storage.dataStore
import myplayground.example.learningq.model.Customer
import myplayground.example.learningq.model.DeliveryOrder
import myplayground.example.learningq.ui.components.CustomBadge
import myplayground.example.learningq.ui.components.CustomButton
import myplayground.example.learningq.ui.components.shimmerBrush
import myplayground.example.learningq.ui.navigation.Screen
import myplayground.example.learningq.ui.theme.LearningQTheme
import myplayground.example.learningq.ui.utils.DateRangeInput
import myplayground.example.learningq.ui.utils.DateRangePickerDialog
import myplayground.example.learningq.ui.utils.ViewModelFactory
import myplayground.example.learningq.ui.utils.parseDate
import myplayground.example.learningq.ui.utils.toFormattedDateString
import java.text.SimpleDateFormat
import java.util.Date

@Composable
fun DriverDeliveryOrderListScreen(
    modifier: Modifier = Modifier,
    vm: DriverDeliveryOrderListViewModel = viewModel(
        factory = ViewModelFactory(
            LocalContext.current.applicationContext as Application,
            Injection.provideRepository(LocalContext.current),
            DatastoreSettings.getInstance(LocalContext.current.dataStore),
        )
    ),
    navController: NavHostController = rememberNavController(),
) {
    val pagingItems = vm.paginationState.collectAsLazyPagingItems()
    val isLoading by vm.isLoading
    val filteredStatus by vm.filteredStatus
    val filteredStartDate by vm.startDate
    val filteredEndDate by vm.endDate

    DriverDeliveryOrderListContent(
        modifier = modifier,
        pagingItems = pagingItems,
        isLoading = isLoading,
        filteredStatus = filteredStatus,
        filteredStartDate = filteredStartDate,
        filteredEndDate = filteredEndDate,
        vm::onEvent,
    ) { deliveryOrderId ->
        navController.navigate(Screen.DriverDeliveryOrderDetail.createRoute(deliveryOrderId))
    }
}

@Composable
fun DriverDeliveryOrderListContent(
    modifier: Modifier = Modifier,
    pagingItems: LazyPagingItems<DeliveryOrder>? = null,
    isLoading: Boolean = false,
    filteredStatus: String? = null,
    filteredStartDate: String? = null,
    filteredEndDate: String? = null,
    onEvent: (event: DriverDeliveryOrderListEvent) -> Unit = {},
    navigateToDeliveryOrderDetail: (deliveryOrderId: String) -> Unit = {},
) {

    val globalManager =
        Injection.provideGlobalManager(LocalContext.current.applicationContext as Application)

    val isFilterModalOpen = remember {
        mutableStateOf(false)
    }


    globalManager.setAppbarTrailing {
        Box(modifier = Modifier.fillMaxSize()) {
            IconButton(
                modifier = Modifier
                    .align(Alignment.CenterEnd)
                    .size(32.dp),
                onClick = {
                    isFilterModalOpen.value = true
                },
            ) {
                Icon(
                    modifier = Modifier.fillMaxSize(),
                    imageVector = Icons.Filled.FilterAlt,
                    contentDescription = "",
                    tint = MaterialTheme.colorScheme.onPrimary,
                )
            }
        }
    }

    if (pagingItems == null || pagingItems.itemCount == 0) {
        EmptyPageView()
    } else {
        LazyColumn(
            modifier = modifier.fillMaxSize(),
        ) {
            pagingItems.let { pagingItem ->

                items(pagingItem.itemCount) { index ->
                    val currentPagingItem = pagingItem[index]!!

                    DeliveryOrderCard(
                        deliveryOrder = currentPagingItem,
                        isLoading = isLoading,
                        onEvent = onEvent,
                        navigateToDeliveryOrderDetail,
                    )

                    Spacer(modifier = Modifier.height(12.dp))
                }

                pagingItem.apply {
                    when {
                        loadState.refresh is LoadState.Loading -> {
                            items(10) {
                                DeliveryOrderCardSkeletonView()
                                Spacer(modifier = Modifier.height(12.dp))
                            }
                        }

                        loadState.refresh is LoadState.Error -> {
                            val error = pagingItem.loadState.refresh as LoadState.Error
                            item {
                                Text(error.toString())
                            }
                        }

                        loadState.append is LoadState.Loading -> {
                            item {
                                Box(modifier = Modifier.fillMaxWidth()) {
                                    CircularProgressIndicator(
                                        modifier = Modifier.align(Alignment.Center),
                                        color = MaterialTheme.colorScheme.onSurface,
                                    )
                                }
                            }
                        }

                        loadState.append is LoadState.Error -> {
                            val error = pagingItem.loadState.append as LoadState.Error
                            item {
                                Text(error.toString())
                            }
                        }
                    }
                }
            }
        }
    }

    if (isFilterModalOpen.value) {
        FilterDialog(
            initialStatus = filteredStatus,
            initialStartDate = filteredStartDate?.let {
                SimpleDateFormat("yyyy-MM-dd").parse(it)?.time
            },
            initialEndDate = filteredEndDate?.let {
                SimpleDateFormat("yyyy-MM-dd").parse(it)?.time
            },
            onDismiss = {
                isFilterModalOpen.value = false
            },
            onFilter = { status, startDate, endDate ->
                onEvent(
                    DriverDeliveryOrderListEvent.SetFilter(
                        status,
                        startDate?.toFormattedDateString(),
                        endDate?.toFormattedDateString()
                    )
                )
                isFilterModalOpen.value = false
            }
        )
    }
}


@Composable
fun FilterDialog(
    initialStatus: String?,
    initialStartDate: Long? = null,
    initialEndDate: Long? = null,
    onDismiss: () -> Unit,
    onFilter: (String?, Long?, Long?) -> Unit
) {
    var selectedStatus by remember { mutableStateOf(initialStatus) }
    var selectedStartDate by remember { mutableStateOf(initialStartDate) }
    var selectedEndDate by remember { mutableStateOf(initialEndDate) }

    var isDateRangePickerOpen by remember { mutableStateOf(false) }

    AlertDialog(
        onDismissRequest = { onDismiss() },
        title = {
            Text(
                text = "Filter",
                style = MaterialTheme.typography.titleMedium,
            )
        },
        text = {
            Column {
                Spacer(modifier = Modifier.height(8.dp))
                Text(
                    text = "Status",
                    style = MaterialTheme.typography.titleSmall,
                )
                Spacer(modifier = Modifier.height(8.dp))
                StatusDropdown(
                    options = listOf(null, "CANCELED", "ONGOING", "DELIVERING", "COMPLETED"),
                    selectedOption = selectedStatus,
                    onOptionSelected = { selectedStatus = it }
                )

                Spacer(modifier = Modifier.height(12.dp))

                Text(
                    text = "Range Tanggal",
                    style = MaterialTheme.typography.titleSmall,
                )
                Spacer(modifier = Modifier.height(8.dp))
                DateRangeInput(
                    selectedStartDate = selectedStartDate,
                    selectedEndDate = selectedEndDate,
                    onDateRangePickerOpen = { isDateRangePickerOpen = true }
                )
//                TextButton(onClick = { isDateRangePickerOpen = true }) {
//                    Text("Select Date Range: ${selectedStartDate?.toFormattedDateString() ?: "Start"} - ${selectedEndDate?.toFormattedDateString() ?: "End"}")
//                }
            }
        },
        confirmButton = {
            Button(
                onClick = { onFilter(selectedStatus, selectedStartDate, selectedEndDate) }
            ) {
                Text("Filter")
            }
        },
        dismissButton = {
            Button(
                onClick = { onDismiss() }
            ) {
                Text("Cancel")
            }
        }
    )

    if (isDateRangePickerOpen) {
        DateRangePickerDialog(
            initialStartDate = selectedStartDate,
            initialEndDate = selectedEndDate,
            onDateRangeSelected = { startDateVal, endDateVal ->
                selectedStartDate = startDateVal
                selectedEndDate = endDateVal
                isDateRangePickerOpen = false
            },
            onDismiss = {
                isDateRangePickerOpen = false
            },
        )
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun StatusDropdown(
    options: List<String?>,
    selectedOption: String?,
    onOptionSelected: (String?) -> Unit
) {
    var expanded by remember { mutableStateOf(false) }
    val context = LocalContext.current

    val displayText = options.map {
        when (it) {
            "CANCELED" -> "Dibatalkan"
            "ONGOING" -> "Menunggu"
            "DELIVERING" -> "Dalam Pengiriman"
            "COMPLETED" -> "Selesai"
            else -> "All"
        }
    }

    var selectedText by remember {
        mutableStateOf(
            displayText[options.indexOf(selectedOption) ?: 0]
        )
    }

    ExposedDropdownMenuBox(
        expanded = expanded,
        onExpandedChange = { expanded = !expanded }
    ) {
        OutlinedTextField(
            value = selectedText,
            onValueChange = {},
            readOnly = true,
            trailingIcon = { ExposedDropdownMenuDefaults.TrailingIcon(expanded = expanded) },
            modifier = Modifier
                .menuAnchor()
                .fillMaxWidth()
        )
        ExposedDropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false }
        ) {
            options.forEachIndexed { index, option ->
                DropdownMenuItem(
                    text = { Text(displayText[index]) },
                    onClick = {
                        selectedText = displayText[index]
                        onOptionSelected(option)
                        expanded = false
                    }
                )
            }
        }
    }
}

@Composable
fun RadioButtonGroup(
    options: List<String?>,
    selectedOption: String?,
    onOptionSelected: (String?) -> Unit
) {
    Column {
        options.forEach { option ->
            val text = option?.let {
                when (it) {
                    "CANCELED" -> "Dibatalkan"
                    "ONGOING" -> "Menunggu"
                    "DELIVERING" -> "Dalam Pengiriman"
                    "COMPLETED" -> "Selesai"
                    else -> ""
                }
            } ?: "All"
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
                    .clickable { onOptionSelected(option) }
            ) {
                RadioButton(
                    selected = (option == selectedOption),
                    onClick = { onOptionSelected(option) }
                )
                Text(
                    text = text,
                    modifier = Modifier.padding(start = 8.dp),
                    style = MaterialTheme.typography.headlineSmall,
                )
            }
        }
    }
}


@Composable
fun EmptyPageView() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            painter = painterResource(id = R.drawable.empty_illustration),
            contentDescription = "Empty Illustration",
            modifier = Modifier.size(200.dp)
        )
        Spacer(modifier = Modifier.height(16.dp))
        Text(
            text = "Data kosong.",
            style = MaterialTheme.typography.titleLarge,
            color = MaterialTheme.colorScheme.onSurface
        )
    }
}

@Composable
fun DeliveryOrderCardSkeletonView() {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(70.dp)
            .clip(MaterialTheme.shapes.medium)
            .background(shimmerBrush()),
    )
}

@Composable
private fun DeliveryOrderCard(
    deliveryOrder: DeliveryOrder,
    isLoading: Boolean = false,
    onEvent: (event: DriverDeliveryOrderListEvent) -> Unit = {},
    navigateToDeliveryOrderDetail: (deliveryOrderId: String) -> Unit = {},
) {
    val isCancelModalOpen = remember {
        mutableStateOf(false)
    }

    val isDeliveryModalOpen = remember {
        mutableStateOf(false)
    }
    val isCompletedModalOpen = remember {
        mutableStateOf(false)
    }

    val isButtonEnabled = remember(deliveryOrder, isLoading) {
        derivedStateOf {
            !isLoading && (deliveryOrder.status != "COMPLETED" && deliveryOrder.status != "CANCELED" && deliveryOrder.status != "RETURNED")
                    && (!deliveryOrder.have_active_delivery || deliveryOrder.status == "DELIVERING")
        }
    }

    Card(
        modifier = Modifier
            .fillMaxWidth()
            .clickable {
                navigateToDeliveryOrderDetail(deliveryOrder.id)
            },
        colors = CardDefaults.cardColors(
            containerColor = MaterialTheme.colorScheme.tertiary,
        ),
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(14.dp, 14.dp, 14.dp, 14.dp),
        ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Text(
                    text = deliveryOrder.invoice_number,
                    style = MaterialTheme.typography.displaySmall,
                    color = MaterialTheme.colorScheme.onSurface,
                )
                Spacer(modifier = Modifier.weight(1F))
                Text(
                    text = parseDate(deliveryOrder.date),
                    style = MaterialTheme.typography.displaySmall,
                    color = MaterialTheme.colorScheme.onSurface,
                )
            }
            Spacer(modifier = Modifier.height(10.dp))
            CustomBadge(
                modifier = Modifier.padding(2.dp),
                backgroundColor = when (deliveryOrder.status) {

                    "CANCELED" -> MaterialTheme.colorScheme.error

                    else -> MaterialTheme.colorScheme.primary
                },
                text = {
                    Text(
                        text = deliveryOrder.statusToIndo().uppercase(),
                        color = MaterialTheme.colorScheme.onPrimary,
                        style = MaterialTheme.typography.bodySmall,
                        modifier = Modifier
                            .padding(4.dp)
                    )
                },
            )
            Spacer(modifier = Modifier.height(10.dp))
            Text(
                text = deliveryOrder.customer?.name ?: "",
                style = MaterialTheme.typography.titleSmall,
                color = MaterialTheme.colorScheme.onSurface,
            )
            Spacer(modifier = Modifier.height(5.dp))
            Text(
                text = deliveryOrder.customer?.address ?: "",
                style = MaterialTheme.typography.bodyMedium,
                color = MaterialTheme.colorScheme.onSurface,
            )
            Spacer(modifier = Modifier.height(10.dp))

            Row(
                modifier = Modifier.fillMaxWidth(),
            ) {
                if (listOf("ONGOING", "DELIVERING").any { it == deliveryOrder.status }) {
                    CustomButton(
                        containerModifier = Modifier.weight(1F),
                        modifier = Modifier.fillMaxWidth(),
                        shape = MaterialTheme.shapes.small,
                        onClick = {
                            isCancelModalOpen.value = true
                        },
                        isLoading = isLoading,
                        enabled = isButtonEnabled.value,
                        colors = ButtonDefaults.buttonColors(
                            containerColor = MaterialTheme.colorScheme.error,
                        ),
                    ) {
                        Text(
                            text = "Batalkan",
                            color = MaterialTheme.colorScheme.onError,
                            style = MaterialTheme.typography.headlineSmall,
                        )
                    }
                    Spacer(modifier = Modifier.width(16.dp))
                }
                CustomButton(
                    containerModifier = Modifier.weight(1F),
                    modifier = Modifier.fillMaxWidth(),
                    shape = MaterialTheme.shapes.small,
                    onClick = {
                        when (deliveryOrder.status) {
                            "ONGOING" -> {
                                isDeliveryModalOpen.value = true
                            }

                            "DELIVERING" -> {
                                isCompletedModalOpen.value = true
                            }
                        }
                    },
                    isLoading = isLoading,
                    enabled = isButtonEnabled.value,
                ) {
                    Text(
                        text = when (deliveryOrder.status) {
                            "ONGOING" -> "KIRIM"
                            "DELIVERING" -> "SELESAI"
                            "SELESAI" -> "SELESAI"
                            else -> ""
                        },
                        color = MaterialTheme.colorScheme.onPrimary,
                        style = MaterialTheme.typography.headlineSmall,
                    )
                }
            }
        }

        if (isCancelModalOpen.value) {
            DialogCancel(
                onDismissRequest = {
                    isCancelModalOpen.value = false
                },
                onConfirmRequest = {
                    onEvent(DriverDeliveryOrderListEvent.Cancel(deliveryOrder.id))
                    isCancelModalOpen.value = false
                },
            )
        }

        if (isDeliveryModalOpen.value) {
            DialogDelivery(
                onDismissRequest = {
                    isDeliveryModalOpen.value = false
                },
                onConfirmRequest = {
                    onEvent(DriverDeliveryOrderListEvent.Delivering(deliveryOrder.id))
                    isDeliveryModalOpen.value = false
                },
            )
        }

        if (isCompletedModalOpen.value) {
            DialogCompleted(
                onDismissRequest = {
                    isCompletedModalOpen.value = false
                },
                onConfirmRequest = {
                    onEvent(DriverDeliveryOrderListEvent.Completed(deliveryOrder.id))
                    isCompletedModalOpen.value = false
                },
            )
        }
    }
}


@Composable
fun DialogCancel(
    onDismissRequest: () -> Unit = {},
    onConfirmRequest: () -> Unit = {},
) {
    AlertDialog(
        containerColor = MaterialTheme.colorScheme.surface,
        onDismissRequest = onDismissRequest,
        icon = {},
        title = {
            Text(
                text = "Cancel",
                style = MaterialTheme.typography.titleSmall,
                color = MaterialTheme.colorScheme.onBackground,
            )
        },
        text = {
            Text(
                text = "Apakah anda ingin yakin?",
                style = MaterialTheme.typography.labelMedium,
                color = MaterialTheme.colorScheme.onBackground,
            )
        },
        confirmButton = {
            Button(onClick = {
                onConfirmRequest()
            }) {
                Text("Yakin")
            }
        },
        dismissButton = {
            Button(onClick = {
                onDismissRequest()
            }) {
                Text("Tidak")
            }
        })
}


@Composable
fun DialogDelivery(
    onDismissRequest: () -> Unit = {},
    onConfirmRequest: () -> Unit = {},
) {
    AlertDialog(
        containerColor = MaterialTheme.colorScheme.surface,
        onDismissRequest = onDismissRequest,
        icon = {},
        title = {
            Text(
                text = "Delivering",
                style = MaterialTheme.typography.titleSmall,
                color = MaterialTheme.colorScheme.onBackground,
            )
        },
        text = {
            Text(
                text = "Apakah anda ingin yakin?",
                style = MaterialTheme.typography.labelMedium,
                color = MaterialTheme.colorScheme.onBackground,
            )
        },
        confirmButton = {
            Button(onClick = {
                onConfirmRequest()
            }) {
                Text("Yakin")
            }
        },
        dismissButton = {
            Button(onClick = {
                onDismissRequest()
            }) {
                Text("Tidak")
            }
        })
}


@Composable
fun DialogCompleted(
    onDismissRequest: () -> Unit = {},
    onConfirmRequest: () -> Unit = {},
) {
    AlertDialog(
        containerColor = MaterialTheme.colorScheme.surface,
        onDismissRequest = onDismissRequest,
        icon = {},
        title = {
            Text(
                text = "Selesai",
                style = MaterialTheme.typography.titleSmall,
                color = MaterialTheme.colorScheme.onBackground,
            )
        },
        text = {
            Text(
                text = "Apakah anda ingin yakin?",
                style = MaterialTheme.typography.labelMedium,
                color = MaterialTheme.colorScheme.onBackground,
            )
        },
        confirmButton = {
            Button(onClick = {
                onConfirmRequest()
            }) {
                Text("Yakin")
            }
        },
        dismissButton = {
            Button(onClick = {
                onDismissRequest()
            }) {
                Text("Tidak")
            }
        })
}

@Preview(showBackground = true, device = Devices.PIXEL_4)
@Preview(showBackground = true, device = Devices.PIXEL_4, uiMode = UI_MODE_NIGHT_YES)
@Composable
fun DriverDeliveryOrderListContentPreview() {
    LearningQTheme {
        DriverDeliveryOrderListContent()
    }
}

@Preview(showBackground = true, device = Devices.PIXEL_4)
@Preview(showBackground = true, device = Devices.PIXEL_4, uiMode = UI_MODE_NIGHT_YES)
@Composable
fun DeliveryOrderCardPreview() {
    LearningQTheme {
        DeliveryOrderCard(
            deliveryOrder = DeliveryOrder(
                id = "uuid",
                supplier_id = "supplierid",
                user_id = "userid",
                invoice_number = "IV/12/25125/2850",
                date = "02/03/2024",
                status = "ONGOING",
                total_price = 10000000.0,
                created_at = "",
                updated_at = "",
                customer = Customer(
                    id = "id",
                    email = "customer@gmail.com",
                    address = "",
                    created_at = "",
                    latitude = 0.0,
                    longitude = 0.0,
                    name = "John Doe",
                    phone = "+6285296968585",
                    updated_at = "",
                ),
            ),
        )
    }
}

