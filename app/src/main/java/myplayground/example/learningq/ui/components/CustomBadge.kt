package myplayground.example.learningq.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp


@Composable
fun CustomBadge(
    modifier: Modifier = Modifier,
    text: @Composable () -> Unit = {},
    backgroundColor: Color = MaterialTheme.colorScheme.primary,
    contentAlignment: Alignment = Alignment.CenterStart,
) {
    Box(
        modifier = modifier
            .clip(CircleShape)
            .border(
                1.dp,
                Color.Transparent,
                CircleShape,
            )
            .background(
                backgroundColor,
                shape = CircleShape
            ),
        contentAlignment = contentAlignment,
    ) {
        text()
    }
}

@Preview
@Composable
fun CustomBadgePreview() {
    var selectedValue by remember { mutableStateOf("") }

    Row {
        CustomBadge(
            text = {
                Text(
                    text = "Lorem",
                    color = MaterialTheme.colorScheme.onPrimary,
                    modifier = Modifier
                        .padding(4.dp)
                )
            }
        )

        Spacer(modifier = Modifier.width(12.dp))

        CustomBadge(
            text = {
                Text(
                    text = "Ipsum",
                    color = MaterialTheme.colorScheme.onPrimary,
                    modifier = Modifier
                        .padding(4.dp)
                )
            }
        )
    }
}
