package myplayground.example.learningq.ui.screens.driver.active

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.location.LocationManager
import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import myplayground.example.learningq.di.Injection
import myplayground.example.learningq.local_storage.LocalStorageManager
import myplayground.example.learningq.model.Class
import myplayground.example.learningq.model.CustomLatLng
import myplayground.example.learningq.model.DeliveryOrder
import myplayground.example.learningq.repository.Repository
import myplayground.example.learningq.ui.utils.getLocation

class DriverActiveViewModel(
    private val context: Context,
    private val repository: Repository,
    private val localStorageManager: LocalStorageManager,
) :
    ViewModel() {
    private val _classState: MutableStateFlow<PagingData<Class>> =
        MutableStateFlow(PagingData.empty())
    private val _currentLocation: MutableState<CustomLatLng?> = mutableStateOf(null)
    private val _currentLines: MutableState<ArrayList<LatLng>> = mutableStateOf(arrayListOf())
    private val _deliveryOrder: MutableState<DeliveryOrder?> = mutableStateOf(null)
    private val locationManager =
        context.getSystemService(Context.LOCATION_SERVICE) as LocationManager

    val currentLocation: State<CustomLatLng?> = _currentLocation
    val deliveryOrder: State<DeliveryOrder?> = _deliveryOrder
    val currentLines: State<ArrayList<LatLng>> = _currentLines

    init {
        onEvent(DriverActiveEvent.Init)
    }

    @SuppressLint("MissingPermission")
    fun onEvent(event: DriverActiveEvent) {
        viewModelScope.launch {
            when (event) {
                is DriverActiveEvent.Init -> {

                    val fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
                    getLocation(fusedLocationClient) { loc ->

                        if (loc != null) {
                            _currentLocation.value = CustomLatLng(
                                lat = loc.latitude,
                                lon = loc.longitude,
                                bear = loc.bearing.toDouble(),
                            )
                        }
                    }

                    _deliveryOrder.value = repository.getActiveDeliveringDeliveryOrder(
                        Injection.provideApiService(localStorageManager)
                    )
                }

                is DriverActiveEvent.OSRM -> {
                    try {
                        if (_deliveryOrder.value != null && _currentLocation.value != null) {
                            val osrm = repository.osrm(
                                _currentLocation.value!!.lon,
                                _deliveryOrder.value!!.customer!!.longitude,
                                _currentLocation.value!!.lat,
                                _deliveryOrder.value!!.customer!!.latitude,
                                Injection.provideRoutingApiService(localStorageManager),
                            )

                            val lines: ArrayList<LatLng> = arrayListOf()

                            osrm.routes[0].legs[0].steps.forEach { step ->
                                step.intersections.forEach { intersection ->
                                    lines.add(
                                        LatLng(
                                            intersection.location[1],
                                            intersection.location[0],
                                        )
                                    )
                                }

//                                lines.add(LatLng(
//                                    step.maneuver!!.location[1],
//                                    step.maneuver!!.location[0],
//                                ))
                            }


                            _currentLines.value = lines
                        }
                    } catch (e: Exception) {
                        Log.i("Exception", e.toString())
                    }
                }
            }
        }
    }

    fun bitmapDescriptorFromVector(
        vectorResId: Int
    ): BitmapDescriptor? {

        // retrieve the actual drawable
        val drawable = ContextCompat.getDrawable(context, vectorResId) ?: return null
        drawable.setBounds(0, 0, drawable.intrinsicWidth, drawable.intrinsicHeight)
        val bm = Bitmap.createBitmap(
            drawable.intrinsicWidth,
            drawable.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )

        // draw it onto the bitmap
        val canvas = android.graphics.Canvas(bm)
        drawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bm)
    }

}


