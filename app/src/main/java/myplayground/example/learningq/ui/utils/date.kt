package myplayground.example.learningq.ui.utils

import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale


fun parseDate(inputDateString: String): String {
    val inputFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
    val outputFormat = SimpleDateFormat("d MMMM yyyy", Locale("id", "ID"))

    val date = inputFormat.parse(inputDateString)
    return outputFormat.format(date)
}


fun Long.toFormattedDateString(): String {
    val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    val date = Date(this)
    return dateFormat.format(date)
}

fun Long.toFormattedFullDateString(): String {
    val dateFormat = SimpleDateFormat("yyyy-MMMM-dd", Locale.getDefault())
    val date = Date(this)
    return dateFormat.format(date)
}