package myplayground.example.learningq.ui.utils

import android.app.DatePickerDialog
import android.text.Layout.Alignment
import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.automirrored.filled.ArrowForward
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.DatePicker
import androidx.compose.material3.DatePickerDialog
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.material3.rememberDatePickerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import myplayground.example.learningq.ui.components.CustomButton
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale


fun Calendar.resetTimeToMidnight(): Calendar {
    this.set(Calendar.HOUR_OF_DAY, 0)
    this.set(Calendar.MINUTE, 0)
    this.set(Calendar.SECOND, 0)
    this.set(Calendar.MILLISECOND, 0)
    return this
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DateRangeInput(
    selectedStartDate: Long?,
    selectedEndDate: Long?,
    onDateRangePickerOpen: () -> Unit
) {
    TextField(
        value = "${selectedStartDate?.toFormattedFullDateString() ?: "Start"} - ${selectedEndDate?.toFormattedFullDateString() ?: "End"}",
        onValueChange = {},
        readOnly = true,
        modifier = Modifier
            .fillMaxWidth()
            .clickable {
                onDateRangePickerOpen()
            },
        enabled = false,
        colors = TextFieldDefaults.textFieldColors(
            disabledTextColor = MaterialTheme.colorScheme.onSurface,
            containerColor = MaterialTheme.colorScheme.surface,
            focusedIndicatorColor = MaterialTheme.colorScheme.primary,
            unfocusedIndicatorColor = MaterialTheme.colorScheme.onSurface.copy(alpha = 0.5f),
            disabledIndicatorColor = MaterialTheme.colorScheme.onSurface.copy(alpha = 0.5f)
        )
    )
}

@Composable
fun DateRangePickerDialog(
    initialStartDate: Long? = null,
    initialEndDate: Long? = null,
    onDateRangeSelected: (Long?, Long?) -> Unit,
    onDismiss: () -> Unit
) {
    val calendar = Calendar.getInstance()
    var startDate by remember { mutableStateOf(initialStartDate) }
    var endDate by remember { mutableStateOf(initialEndDate) }
    var displayedMonth by remember { mutableStateOf(calendar.get(Calendar.MONTH)) }
    var displayedYear by remember { mutableStateOf(calendar.get(Calendar.YEAR)) }

    fun formatDate(dateInMillis: Long): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val date = Date(dateInMillis)
        return dateFormat.format(date)
    }

    AlertDialog(
        onDismissRequest = onDismiss,
        confirmButton = {
            Row(
                modifier = Modifier.fillMaxWidth(),
            ) {
                Button(
                    modifier = Modifier.weight(1F),
                    colors = ButtonDefaults.buttonColors(
                        containerColor = MaterialTheme.colorScheme.error,
                    ),
                    onClick = {
                        startDate = null
                        endDate = null
                    }
                ) {
                    Text("Reset")
                }
                Spacer(modifier = Modifier.width(8.dp))
                OutlinedButton(
                    modifier = Modifier.weight(1F),
                    onClick = { onDismiss() }
                ) {
                    Text("Cancel")
                }
            }
        },
        dismissButton = {

            Button(
                modifier = Modifier.fillMaxWidth(),
                onClick = {
                        onDateRangeSelected(startDate, endDate)
                        onDismiss()
                },
            ) {
                Text("Select")
            }
        },
        text = {
            Column(
                horizontalAlignment = androidx.compose.ui.Alignment.CenterHorizontally,
                modifier = Modifier.padding(16.dp)
            ) {
                Text(
                    text = "Pilih Range Tanggal",
                    style = MaterialTheme.typography.titleMedium,
                    fontWeight = FontWeight.Bold
                )
                Spacer(modifier = Modifier.height(8.dp))

                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 8.dp),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    IconButton(onClick = {
                        displayedMonth = if (displayedMonth == 0) 11 else displayedMonth - 1
                        if (displayedMonth == 11) {
                            displayedYear -= 1
                        }
                    }) {
                        Icon(
                            imageVector = Icons.AutoMirrored.Filled.ArrowBack,
                            contentDescription = null
                        )
                    }

                    Text(
                        text = SimpleDateFormat("MMMM yyyy", Locale.getDefault())
                            .format(calendar.apply {
                                set(Calendar.MONTH, displayedMonth)
                                set(Calendar.YEAR, displayedYear)
                            }.time),
                        style = MaterialTheme.typography.titleMedium,
                        fontWeight = FontWeight.Bold
                    )

                    IconButton(onClick = {
                        displayedMonth = if (displayedMonth == 11) 0 else displayedMonth + 1
                        if (displayedMonth == 0) {
                            displayedYear += 1
                        }
                    }) {
                        Icon(
                            imageVector = Icons.AutoMirrored.Filled.ArrowForward,
                            contentDescription = null
                        )
                    }
                }

                CalendarView(
                    displayedMonth = displayedMonth,
                    displayedYear = displayedYear,
                    startDate = startDate,
                    endDate = endDate,
                    onDateSelected = { dateInMillis ->
                        if (startDate == null || (startDate != null && endDate != null)) {
                            startDate = dateInMillis
                            endDate = null
                        } else {
                            if (dateInMillis < startDate!!) {
                                startDate = dateInMillis
                            } else {
                                endDate = dateInMillis
                            }
                        }
                    }
                )
            }
        }
    )
}

@Composable
fun CalendarView(
    displayedMonth: Int,
    displayedYear: Int,
    startDate: Long?,
    endDate: Long?,
    onDateSelected: (Long) -> Unit
) {
    val calendar = Calendar.getInstance().apply {
        set(Calendar.MONTH, displayedMonth)
        set(Calendar.YEAR, displayedYear)
        set(Calendar.DAY_OF_MONTH, 1)
    }
    val daysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH)
    val dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1 // to start with Sunday

    Column {
        Row(
            horizontalArrangement = Arrangement.SpaceEvenly,
            modifier = Modifier.fillMaxWidth()
        ) {
            listOf("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat").forEach { day ->
                Text(
                    text = day,
                    modifier = Modifier.weight(1f),
                    textAlign = TextAlign.Center,
                    fontWeight = FontWeight.Bold,
                    color = MaterialTheme.colorScheme.primary
                )
            }
        }

        var currentDay = 1
        val rows = (dayOfWeek + daysInMonth + 6) / 7

        for (row in 0 until rows) {
            Row(
                horizontalArrangement = Arrangement.SpaceEvenly,
                modifier = Modifier.fillMaxWidth()
            ) {
                for (col in 0..6) {
                    val day =
                        if (row == 0 && col < dayOfWeek || currentDay > daysInMonth) null else currentDay++

                    Box(
                        modifier = Modifier
                            .weight(1f)
                            .aspectRatio(1f)
                            .padding(4.dp)
                            .background(
                                color = when {
                                    day == null -> Color.Transparent
                                    startDate != null && endDate != null &&
                                            calendar.apply {
                                                set(Calendar.DAY_OF_MONTH, day)
                                                resetTimeToMidnight()
                                            }.timeInMillis in startDate..endDate -> MaterialTheme.colorScheme.secondary

                                    startDate != null && endDate == null &&
                                            calendar.apply {
                                                set(Calendar.DAY_OF_MONTH, day)
                                                resetTimeToMidnight()
                                            }.timeInMillis == startDate -> MaterialTheme.colorScheme.secondary

                                    else -> Color.Transparent
                                },
                                shape = MaterialTheme.shapes.small
                            )
                            .clickable(enabled = day != null) {
                                if (day != null) {
                                    calendar.set(Calendar.DAY_OF_MONTH, day)
                                    onDateSelected(calendar.resetTimeToMidnight().timeInMillis)
                                }
                            },
                        contentAlignment = androidx.compose.ui.Alignment.Center,
                    ) {
                        Text(
                            text = day?.toString() ?: "",
                            style = MaterialTheme.typography.bodyLarge,
                            color = when {
                                day == null -> MaterialTheme.colorScheme.onBackground
                                startDate != null && endDate != null &&
                                        calendar.apply {
                                            set(Calendar.DAY_OF_MONTH, day)
                                            resetTimeToMidnight()
                                        }.timeInMillis in startDate..endDate -> MaterialTheme.colorScheme.onSecondary

                                startDate != null && endDate == null &&
                                        calendar.apply {
                                            set(Calendar.DAY_OF_MONTH, day)
                                            resetTimeToMidnight()
                                        }.timeInMillis == startDate -> MaterialTheme.colorScheme.onSecondary

                                else -> MaterialTheme.colorScheme.onBackground
                            }
                        )
                    }
                }
            }
        }
    }
}

//
//@OptIn(ExperimentalMaterial3Api::class)
//@Composable
//fun DateRangePicker(
//    initialStartDate: Long?,
//    initialEndDate: Long?,
//    onDateRangeSelected: (Long, Long) -> Unit,
//    onDismiss: () -> Unit
//) {
//    val context = LocalContext.current
//    val calendar = remember { Calendar.getInstance() }
//    var startDate by remember { mutableStateOf(initialStartDate) }
//    var endDate by remember { mutableStateOf(initialEndDate) }
//
//    DatePickerDialog(
//        onDismissRequest = onDismiss,
//        confirmButton = {
//            Button(onClick = {
//                if (startDate != null && endDate != null) {
//                    onDateRangeSelected(startDate!!, endDate!!)
//                    onDismiss()
//                }
//            }) {
//                Text("Select")
//            }
//        },
//        dismissButton = {
//            Button(onClick = onDismiss) {
//                Text("Cancel")
//            }
//        }
//    ) {
//        DatePicker(
//            state = rememberDatePickerState(
//                initialSelectedDateMillis = startDate,
//                onDateSelected = { dateMillis ->
//                    if (startDate == null || (startDate != null && endDate != null)) {
//                        startDate = dateMillis
//                        endDate = null
//                    } else {
//                        if (dateMillis != null && dateMillis < startDate!!) {
//                            startDate = dateMillis
//                        } else {
//                            endDate = dateMillis
//                        }
//                    }
//                }
//            )
//        )
//    }
//}

//
//@Composable
//fun DateRangePicker(
//    initialStartDate: Long?,
//    initialEndDate: Long?,
//    onDateRangeSelected: (startDate: Long?, endDate: Long?) -> Unit
//) {
//    val context = LocalContext.current
//    val dateFormat = remember { SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()) }
//
//    var startDate by remember { mutableStateOf(initialStartDate) }
//    var endDate by remember { mutableStateOf(initialEndDate) }
//
//    val calendar = Calendar.getInstance()
//
//    val startDatePickerDialog = DatePickerDialog(
//        context,
//        { _, year, month, dayOfMonth ->
//            calendar.set(year, month, dayOfMonth)
//            startDate = calendar.timeInMillis
//            onDateRangeSelected(startDate, endDate)
//        },
//        calendar.get(Calendar.YEAR),
//        calendar.get(Calendar.MONTH),
//        calendar.get(Calendar.DAY_OF_MONTH)
//    )
//
//    val endDatePickerDialog = DatePickerDialog(
//        context,
//        { _, year, month, dayOfMonth ->
//            calendar.set(year, month, dayOfMonth)
//            endDate = calendar.timeInMillis
//            onDateRangeSelected(startDate, endDate)
//        },
//        calendar.get(Calendar.YEAR),
//        calendar.get(Calendar.MONTH),
//        calendar.get(Calendar.DAY_OF_MONTH)
//    )
//
//    Column(
//        modifier = Modifier
//            .fillMaxWidth()
//            .padding(8.dp)
//    ) {
//        Text(
//            text = "Pilih Range Tanggal",
//            style = MaterialTheme.typography.titleMedium,
//            modifier = Modifier.padding(bottom = 8.dp)
//        )
//        Row(
//            modifier = Modifier
//                .fillMaxWidth()
//                .padding(vertical = 8.dp)
//        ) {
//            Text(
//                text = startDate?.let { dateFormat.format(it) } ?: "Tanggal Mulai",
//                style = MaterialTheme.typography.bodyMedium,
//                modifier = Modifier
//                    .weight(1f)
//                    .clickable { startDatePickerDialog.show() }
//                    .padding(8.dp)
//            )
//            Spacer(modifier = Modifier.width(16.dp))
//            Text(
//                text = endDate?.let { dateFormat.format(it) } ?: "Tanggal Berakhir",
//                style = MaterialTheme.typography.bodyMedium,
//                modifier = Modifier
//                    .weight(1f)
//                    .clickable { endDatePickerDialog.show() }
//                    .padding(8.dp)
//            )
//        }
//    }
//}
