package myplayground.example.learningq.ui.screens.driver.delivery_order_detail

import android.app.Application
import android.content.res.Configuration.UI_MODE_NIGHT_YES
import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.AsyncImage
import myplayground.example.learningq.R
import myplayground.example.learningq.di.Injection
import myplayground.example.learningq.local_storage.DatastoreSettings
import myplayground.example.learningq.local_storage.dataStore
import myplayground.example.learningq.model.DeliveryOrder
import myplayground.example.learningq.ui.theme.LearningQTheme
import myplayground.example.learningq.ui.utils.ViewModelFactory
import myplayground.example.learningq.ui.utils.debugPlaceholder
import myplayground.example.learningq.ui.utils.formatCurrency

@Composable
fun DriverDeliveryOrderDetailScreen(
    modifier: Modifier = Modifier,
    vm: DriverDeliveryOrderDetailViewModel = viewModel(
        factory = ViewModelFactory(
            LocalContext.current.applicationContext as Application,
            Injection.provideRepository(LocalContext.current),
            DatastoreSettings.getInstance(LocalContext.current.dataStore),
        )
    ),
    deliveryOrderId: String = "",
) {
    val deliveryOrder by vm.deliveryOrder

    LaunchedEffect(deliveryOrderId) {
        vm.onEvent(DriverDeliveryOrderDetailEvent.GetDeliveryOrder(deliveryOrderId))
    }

    DriverDeliveryOrderDetailContent(
        modifier = modifier,
        deliveryOrder = deliveryOrder,
        vm::onEvent,
//        classesPagingItem = classesPagingItem
    )
}

@Composable
fun DriverDeliveryOrderDetailContent(
    modifier: Modifier = Modifier,
    deliveryOrder: DeliveryOrder? = null,
    onEvent: (event: DriverDeliveryOrderDetailEvent) -> Unit = {},
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(MaterialTheme.colorScheme.background)
            .padding(16.dp)
            .verticalScroll(rememberScrollState())
    ) {
        // Display additional information here based on currentLocation, deliveryOrder, or lines
        deliveryOrder?.customer?.let { customer ->
            Text(
                text = deliveryOrder.invoice_number,
                style = MaterialTheme.typography.titleSmall,
                color = MaterialTheme.colorScheme.onBackground,
            )
            Spacer(modifier = Modifier.height(5.dp))
            Text(
                text = customer.name,
                style = MaterialTheme.typography.titleSmall,
                color = MaterialTheme.colorScheme.onBackground,
            )
            Spacer(modifier = Modifier.height(5.dp))
            Text(
                text = customer.address,
                style = MaterialTheme.typography.bodySmall,
                color = MaterialTheme.colorScheme.onBackground,
            )
            Spacer(modifier = Modifier.height(5.dp))
            Row(
                modifier = Modifier.fillMaxWidth(),
                Arrangement.SpaceBetween,
            ) {
                Text(
                    text = "${deliveryOrder.items?.size ?: "0"} Barang",
                    style = MaterialTheme.typography.displaySmall,
                    color = MaterialTheme.colorScheme.onBackground,
                )
                Text(
                    text = deliveryOrder.date,
                    style = MaterialTheme.typography.displaySmall,
                    color = MaterialTheme.colorScheme.onBackground,
                )
            }
            Spacer(modifier = Modifier.height(12.dp))


            deliveryOrder.items?.map {
                val productUnit = it.product_unit!!
                val product = productUnit.product!!
                val unit = productUnit.unit!!

                Card(
                    modifier = Modifier.fillMaxWidth(),
                    colors = CardDefaults.cardColors(
                        containerColor = MaterialTheme.colorScheme.tertiary,
                    ),
                ) {
                    Row(
                        modifier = Modifier.padding(8.dp, 12.dp)
                    ) {
                        AsyncImage(
                            model = product.image_file?.link ?: "",
                            contentDescription = product.name ?: "",
                            placeholder = debugPlaceholder(debugPreview = R.drawable.avatar_placeholder),
                            modifier = Modifier.width(100.dp),
                        )
                        Column(
                            modifier = Modifier.fillMaxWidth(),
                        ) {
                            Text(
                                text = product.name,
                                style = MaterialTheme.typography.displayMedium,
                                color = MaterialTheme.colorScheme.onBackground,
                            )
                            Spacer(modifier = Modifier.height(4.dp))
                            Row(
                                modifier = Modifier.fillMaxWidth(),
                            ) {
                                Text(
                                    text = unit.name,
                                    style = MaterialTheme.typography.headlineSmall,
                                    color = MaterialTheme.colorScheme.onBackground,
                                )
                                Spacer(modifier = Modifier.weight(1F))
                                Text(
                                    text = "x${it.qty}",
                                    style = MaterialTheme.typography.headlineSmall,
                                    color = MaterialTheme.colorScheme.onBackground,
                                )
                            }
                            Spacer(modifier = Modifier.height(8.dp))
                            Row(
                                modifier = Modifier.fillMaxWidth(),
                            ) {
                                Spacer(modifier = Modifier.weight(1F))
                                Text(
                                    text = formatCurrency(it.price_per_unit - it.discount_per_unit),
                                    style = MaterialTheme.typography.displaySmall,
                                    color = MaterialTheme.colorScheme.onBackground,
                                )
                            }
                        }
                    }
                }
                Spacer(modifier = Modifier.height(12.dp))

            }


            deliveryOrder.images?.let { images ->
                var count = 0
                Card(
                    modifier = Modifier
                        .fillMaxWidth(),
                    colors = CardDefaults.cardColors(
                        containerColor = MaterialTheme.colorScheme.tertiary,
                    ),
                ) {
                    Column(
                        modifier = Modifier
                            .padding(12.dp, 12.dp)
                    ) {
                        Text(
                            text = "Foto",
                            style = MaterialTheme.typography.displayMedium,
                            color = MaterialTheme.colorScheme.onBackground,
                        )
                        Spacer(modifier = Modifier.height(4.dp))

                        GridComponent(
                            images,
                            3,
                        ) { image ->
                            AsyncImage(
                                model = image.file?.link ?: "",
                                contentDescription = image.id ?: "",
                                placeholder = debugPlaceholder(debugPreview = R.drawable.avatar_placeholder),
                                modifier = Modifier
                                    .aspectRatio(1F)
                                    .weight(1F),
                            )
                        }
                    }
                }
            }
        }
    }
}

@Preview(showBackground = true, device = Devices.PIXEL_4)
@Preview(showBackground = true, device = Devices.PIXEL_4, uiMode = UI_MODE_NIGHT_YES)
@Composable
fun DriverDeliveryOrderDetailContentPreview() {
    LearningQTheme {
        DriverDeliveryOrderDetailContent()
    }
}


@Composable
fun <T> GridComponent(
    items: List<T>,
    columns: Int,
    itemContent: @Composable (item: T) -> Unit
) {
    Column(modifier = Modifier.fillMaxWidth()) {
        val chunkedItems = items.chunked(columns)
        chunkedItems.forEachIndexed { index, rowItems ->
            Row(modifier = Modifier.fillMaxWidth()) {
                rowItems.forEach { item ->
                    itemContent(item)

                    Spacer(modifier = Modifier.width(4.dp))
                }
                // Fill remaining space with empty boxes in the last row
                if (index == chunkedItems.size - 1 && rowItems.size < columns) {
                    repeat(columns - rowItems.size) {
                        Box(
                            modifier = Modifier
                                .weight(1F)
                                .background(Color.Transparent)
                        ) {
                        }
                        Spacer(modifier = Modifier.width(4.dp))
                    }
                }
            }
        }
    }
}